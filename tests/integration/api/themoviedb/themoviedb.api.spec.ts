import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'

import { TheMovieDBApi } from '../../../../src/shared/api/themoviedb'
import { expectedSearchResultApi, expectedSearchResultsNoData } from '../../snapshots/search'
import { expectedPlayingNowResultsApi } from '../../snapshots/movies'
import {
  themoviedbApiSearchResponse,
  themoviedbApiSearchResponseNoData,
  themoviedbApiVideosResponse,
  themoviedbApiBadVideosResponse,
} from '../../mock-data/themoviedb.mocks'

const sandbox = sinon.createSandbox()
const themoviedbApi = new TheMovieDBApi()

describe('TheMovieDB Api', () => {
  afterEach(() => {
    sandbox.restore()
  })

  it('should search movies', async () => {
    sandbox.stub(request, 'get').yields(undefined, { statusCode: 200 }, themoviedbApiSearchResponse)
    const searchResult = await themoviedbApi.searchMovies('fight')
    expect(searchResult).to.deep.equal(expectedSearchResultApi)
  })

  it('should handle search movies when search returns no data', async () => {
    sandbox
      .stub(request, 'get')
      .yields(undefined, { statusCode: 200 }, themoviedbApiSearchResponseNoData)
    const searchResult = await themoviedbApi.searchMovies('fight')
    expect(searchResult).to.deep.equal(expectedSearchResultsNoData)
  })

  it('should get playing now movies', async () => {
    sandbox.stub(request, 'get').yields(undefined, { statusCode: 200 }, themoviedbApiSearchResponse)
    const searchResult = await themoviedbApi.getPlayingNow()
    expect(searchResult).to.deep.equal(expectedPlayingNowResultsApi)
  })

  it('should get movies official youtube trailer key', async () => {
    sandbox.stub(request, 'get').yields(undefined, { statusCode: 200 }, themoviedbApiVideosResponse)
    const key = await themoviedbApi.getOfficialTrailerYoutubeKey('320288')
    expect(key).to.deep.equal('QWbMckU3AOQ')
  })

  it('should return first trailer found when an official trailer is not found', async () => {
    sandbox
      .stub(request, 'get')
      .yields(undefined, { statusCode: 200 }, themoviedbApiBadVideosResponse)
    const key = await themoviedbApi.getOfficialTrailerYoutubeKey('320288')
    expect(key).to.deep.equal('QWbMckU3AOQ')
  })

  it('should handle returning youtube official trailer when passing invalid arguments', async () => {
    sandbox.stub(request, 'get').yields(undefined, { statusCode: 200 }, { results: [] })
    const key = await themoviedbApi.getOfficialTrailerYoutubeKey()
    expect(key).to.deep.equal(undefined)
  })

  it('should return proper urls', () => {
    const url = themoviedbApi.getUrl('somequery?someParam=1')
    expect(url).to.deep.equal(
      'https://api.themoviedb.org/3/somequery?someParam=1&api_key=a_movide_db_key',
    )
    const url2 = themoviedbApi.getUrl('somequery')
    expect(url2).to.deep.equal('https://api.themoviedb.org/3/somequery?api_key=a_movide_db_key')
  })

  it('should handle parsing movies when they dont have all the data', () => {
    // eslint-disable-next-line @typescript-eslint/camelcase
    const movies = [{ id: '1', title: 'movie1', original_title: 'original_title' }]
    const parsedMovies = themoviedbApi.parseMovies(movies)
    expect(parsedMovies[0]).to.deep.equal({
      id: '1',
      title: 'movie1',
      originalTitle: 'original_title',
      posterPath: undefined,
      genres: [],
      overview: undefined,
      releaseDate: undefined,
      year: '',
    })
  })

  it('should parse movie genres', () => {
    const genres = themoviedbApi.parseMovieGenres()
    expect(genres).to.deep.equal([])

    const genres2 = themoviedbApi.parseMovieGenres([28, 10402])
    expect(genres2).to.deep.equal(['Action', 'Music'])
  })
})
