import { ids } from '../fixtures-ids'

export const expectedRoles = [
  {
    permissions: ['create:movie'],
    _id: ids.userRoles.admin,
    name: 'admin',
  },
  {
    permissions: ['read:movie'],
    _id: ids.userRoles.user,
    name: 'user',
  },
]
