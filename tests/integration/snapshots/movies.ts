import { ids } from '../fixtures-ids'

export const expectedMovieByTmdbId = {
  genres: ['Mystery', 'Thriller'],
  _id: ids.movies.mementoId,
  title: 'Memento',
  originalTitle: 'Memento',
  posterPath: 'memento.jpg',
  overview: 'Awesome movie',
  releaseDate: '2000-07-04',
  year: '2000',
  tmdbId: '11',
}

export const expectedSaveAllTmdbMovies = [
  {
    genres: ['Mystery', 'Thriller'],
    _id: ids.movies.mementoId,
    title: 'Memento',
    originalTitle: 'Memento',
    posterPath: 'memento.jpg',
    overview: 'Awesome movie',
    releaseDate: '2000-07-04',
    year: '2000',
    tmdbId: '11',
  },
  { genres: [], tmdbId: '33', title: 'The Help' },
]

export const expectedPlayingNowResults = [
  {
    genres: ['Drama'],
    tmdbId: '550',
    title: 'The Fight Club',
    originalTitle: 'The Fight Club',
    posterPath: 'adw6Lq9FiC9zjYEpOqfq03ituwp.jpg',
    overview: 'A ticking-time-bomb...',
    releaseDate: '1999-10-15',
    year: '1999',
  },
  {
    genres: ['Comedy', 'Drama'],
    tmdbId: '482986',
    title: 'The Fight',
    originalTitle: 'The Fight',
    posterPath: 'uXrvRFcy7YJSyhtPpeaxKdXYHd3.jpg',
    overview: 'Tina lives in a quiet seaside...',
    releaseDate: '2018-10-17',
    year: '2018',
  },
]

export const expectedPlayingNowResultsApi = [
  {
    id: 550,
    title: 'The Fight Club',
    originalTitle: 'The Fight Club',
    posterPath: 'adw6Lq9FiC9zjYEpOqfq03ituwp.jpg',
    genres: ['Drama'],
    overview: 'A ticking-time-bomb...',
    releaseDate: '1999-10-15',
    year: '1999',
  },
  {
    id: 482986,
    title: 'The Fight',
    originalTitle: 'The Fight',
    posterPath: 'uXrvRFcy7YJSyhtPpeaxKdXYHd3.jpg',
    genres: ['Comedy', 'Drama'],
    overview: 'Tina lives in a quiet seaside...',
    releaseDate: '2018-10-17',
    year: '2018',
  },
]
