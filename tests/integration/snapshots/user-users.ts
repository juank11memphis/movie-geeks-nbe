import { ids } from '../fixtures-ids'

export const expectedUserUsers = {
  _id: ids.userUsers.sammetLandeId,
  user: ids.users.sammetId,
  comparandUser: ids.users.landeId,
  score: 1,
  movies: [
    {
      id: ids.movies.bloodDiamondId,
      points: 1,
    },
  ],
}

export const expectedUserUsersNewRating = [
  {
    movies: [
      { id: ids.movies.sixthSenseId, points: 2 },
      { id: ids.movies.easternPromisesId, points: 1 },
    ],
    user: ids.users.harrisId,
    comparandUser: ids.users.janickId,
    score: 3,
  },
  {
    movies: [
      { id: ids.movies.sixthSenseId, points: 2 },
      { id: ids.movies.easternPromisesId, points: 1 },
    ],
    user: ids.users.janickId,
    comparandUser: ids.users.murrayId,
    score: 3,
  },
]

export const expectedUserUsersRatingModified = [
  {
    movies: [
      { id: ids.movies.sixthSenseId, points: 0 },
      { id: ids.movies.easternPromisesId, points: 2 },
    ],
    user: ids.users.harrisId,
    comparandUser: ids.users.murrayId,
    score: 2,
  },
  {
    movies: [{ id: ids.movies.sixthSenseId, points: 0 }],
    user: ids.users.harrisId,
    comparandUser: ids.users.janickId,
    score: 0,
  },
]

export const expectedUserUsersRatingDeleted1 = {
  movies: [{ id: ids.movies.sixthSenseId, points: 2 }],
  user: ids.users.harrisId,
  comparandUser: ids.users.murrayId,
  score: 2,
}
