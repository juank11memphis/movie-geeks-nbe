import { ids } from '../fixtures-ids'

export const expectedRatedMovies = [
  {
    rating: 10,
    saved: false,
    user: ids.users.sanId,
    movie: ids.movies.requiemId,
  },
  {
    rating: 9,
    saved: false,
    user: ids.users.sanId,
    movie: ids.movies.sevenId,
  },
]

export const expectedJoseCatalog = {
  watchlist: [
    {
      saved: true,
      user: ids.users.joseId,
      movie: ids.movies.mementoId,
    },
    {
      saved: true,
      user: ids.users.joseId,
      movie: ids.movies.snatchId,
    },
  ],
  collection: [
    {
      saved: false,
      _id: ids.userMovies.joseSevenId,
      user: ids.users.joseId,
      movie: ids.movies.sevenId,
      rating: 9,
    },
    {
      saved: false,
      _id: ids.userMovies.joseRequiemId,
      user: ids.users.joseId,
      movie: ids.movies.requiemId,
      rating: 10,
    },
  ],
}

export const expectedUserMoviesUserNull = [
  {
    user: '',
    movie: ids.movies.requiemId,
    rating: undefined,
    saved: false,
    movieDetail: { _id: ids.movies.requiemId },
  },
  {
    user: '',
    movie: ids.movies.sevenId,
    rating: undefined,
    saved: false,
    movieDetail: { _id: ids.movies.sevenId },
  },
]

export const expectedUserMoviesUserAll = [
  {
    user: ids.users.joseId,
    movie: ids.movies.sevenId,
    rating: 9,
    saved: false,
    movieDetail: { _id: ids.movies.sevenId },
  },
  {
    user: ids.users.joseId,
    movie: ids.movies.snatchId,
    rating: undefined,
    saved: true,
    movieDetail: { _id: ids.movies.snatchId },
  },
]

export const expectedUserMoviesUserSome = [
  {
    user: ids.users.joseId,
    movie: ids.movies.sevenId,
    rating: 9,
    saved: false,
    movieDetail: { _id: ids.movies.sevenId },
  },
  {
    user: ids.users.joseId,
    movie: ids.movies.fightClubId,
    rating: undefined,
    saved: false,
    movieDetail: { _id: ids.movies.fightClubId },
  },
]

export const expectedPlayingNowMovies = [
  {
    user: ids.users.juanId,
    rating: undefined,
    saved: false,
    movieDetail: { title: 'The Fight Club' },
  },
  {
    user: ids.users.juanId,
    rating: undefined,
    saved: false,
    movieDetail: { title: 'The Fight' },
  },
]

export const expectedSearch = {
  page: 1,
  totalResults: 1232,
  totalPages: 62,
  userMovies: [
    {
      user: ids.users.juanId,
      rating: undefined,
      saved: false,
      movieDetail: { title: 'The Fight Club' },
    },
    {
      user: ids.users.juanId,
      rating: undefined,
      saved: false,
      movieDetail: { title: 'The Fight' },
    },
  ],
}

export const expectedWatchlistCurrentUserIdAndComparandUserId = [
  {
    user: ids.users.juanId,
    saved: true,
    movieDetail: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    movieDetail: { title: 'The Machinist' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: undefined,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    movieDetail: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
  {
    user: ids.users.juanId,
    saved: true,
    movieDetail: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
]

export const expectedCollectionCurrentUserIdAndComparandUserId = [
  {
    user: ids.users.joseId,
    saved: false,
    rating: 10,
    movieDetail: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.juanId + '',
      rating: undefined,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: false,
    rating: 9,
    movieDetail: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.juanId + '',
      rating: undefined,
      saved: true,
    },
  },
]

export const expectedCollectionAs = [
  {
    user: ids.users.joseId,
    saved: false,
    rating: 10,
    movieDetail: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 10,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: false,
    rating: 9,
    movieDetail: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: true,
    rating: undefined,
    movieDetail: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: ids.users.joseId,
    saved: true,
    rating: undefined,
    movieDetail: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
]

export const expectedCollectionAsMainUserNull = [
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Requiem for a Dream' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 10,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Seven' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 9,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 7,
      saved: false,
    },
  },
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.sanId + '',
      rating: 4,
      saved: false,
    },
  },
]

export const expectedWatchlistAsMainUserNull = [
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
  {
    user: '',
    saved: false,
    rating: undefined,
    movieDetail: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
]

export const expectedWatchlistAs = [
  {
    user: ids.users.sanId,
    saved: false,
    rating: 7,
    movieDetail: { title: 'Snatch' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
  {
    user: ids.users.sanId,
    saved: false,
    rating: 4,
    movieDetail: { title: 'Memento' },
    comparandUser: {
      userId: ids.users.joseId + '',
      rating: undefined,
      saved: true,
    },
  },
]
