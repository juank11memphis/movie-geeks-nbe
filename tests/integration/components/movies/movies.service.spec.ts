import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import { MoviesService } from '../../../../src/components/movies/movies.service'
import { TheMovieDBApi } from '../../../../src/shared/api/themoviedb'
import {
  expectedMovieByTmdbId,
  expectedSaveAllTmdbMovies,
  expectedPlayingNowResults,
} from '../../snapshots/movies'
import { themoviedbApiPlayingNowResponse } from '../../mock-data/themoviedb.mocks'
import { ids } from '../../fixtures-ids'

const sandbox = sinon.createSandbox()
const mementoTmdbId = '11'
const moviesService = new MoviesService()

describe('MoviesService', () => {
  beforeEach((done: any) => {
    void testsSetup(done)
  })

  afterEach(() => {
    sandbox.restore()
  })

  it('should get playing now movies', async () => {
    sandbox
      .stub(TheMovieDBApi.prototype, 'getPlayingNow')
      .returns(Promise.resolve(themoviedbApiPlayingNowResponse))
    const result = await moviesService.getPlayingNow()
    expect(result).to.containSubset(expectedPlayingNowResults)
  })

  it('should handle get playing now movies when the api returns nothing', async () => {
    sandbox.stub(TheMovieDBApi.prototype, 'getPlayingNow').returns(Promise.resolve([]))
    const result = await moviesService.getPlayingNow()
    expect(result).to.deep.equal([])
  })

  it('should get movies by id', async () => {
    const movie = await moviesService.getById(ids.movies.mementoId)
    expect(movie.toObject()).to.containSubset(expectedMovieByTmdbId)
  })

  it('should get movies by tmdb api id', async () => {
    const movie = await moviesService.getByTmdbId(mementoTmdbId)
    expect(movie.toObject()).to.containSubset(expectedMovieByTmdbId)
  })

  it('should throw an exception if a movie with tmdb api id does not exists', async () => {
    try {
      await moviesService.getByTmdbId('bad-id')
    } catch (error) {
      expect(error.message).to.deep.equal(`Movie with tmdb bad-id not found in database`)
    }
  })

  it('should return movies by id or tmdb id', async () => {
    let movie = await moviesService.getByIdOrTmdbId(ids.movies.mementoId)
    expect(movie.toObject()).to.containSubset(expectedMovieByTmdbId)
    movie = await moviesService.getByIdOrTmdbId(mementoTmdbId)
    expect(movie.toObject()).to.containSubset(expectedMovieByTmdbId)
  })

  it('should create new movies', async () => {
    const newMovie = {
      title: 'new_movie',
      tmdbId: '11',
    }
    const dbMovie = await moviesService.create(newMovie)
    expect(dbMovie.toObject()).to.containSubset({
      title: 'new_movie',
      tmdbId: '11',
    })
  })

  it('should create new movies based on an array of tmdb movies array', async () => {
    const tmdbMovies = [
      {
        id: '11',
        title: 'Memento',
      },
      {
        id: '33',
        title: 'The Help',
      },
    ]
    const dbMovies = await moviesService.saveAllTMDBMovies(tmdbMovies)
    expect(dbMovies).to.containSubset(expectedSaveAllTmdbMovies)
  })

  it('should handle create new movies based on an empty array of tmdb movies', async () => {
    const dbMovies = await moviesService.saveAllTMDBMovies(undefined)
    expect(dbMovies).to.deep.equal([])
  })

  it('should load movies by ids', async () => {
    const dbMovies = await moviesService.loadByIds([
      ids.movies.machinistId,
      ids.movies.mementoId,
      ids.movies.fightClubId,
    ])
    expect(dbMovies.length).to.equal(3)
    expect(dbMovies[0]._id + '').to.deep.equal(ids.movies.machinistId + '')
    expect(dbMovies[1]._id + '').to.deep.equal(ids.movies.mementoId + '')
    expect(dbMovies[2]._id + '').to.deep.equal(ids.movies.fightClubId + '')
  })

  it('should get official youtube trailer key if it already exists in the database', async () => {
    const key = await moviesService.getYoutubeOfficialTrailerKey(ids.movies.machinistId)
    expect(key).to.deep.equal('1133')
  })

  it('should get official youtube trailer key from themovidedbApi', async () => {
    sandbox
      .stub(TheMovieDBApi.prototype, 'getOfficialTrailerYoutubeKey')
      .returns(Promise.resolve('1133'))
    const key = await moviesService.getYoutubeOfficialTrailerKey(ids.movies.madMaxId)
    expect(key).to.deep.equal('1133')
    const movie = await moviesService.getById(ids.movies.madMaxId)
    expect(movie.youtubeTrailerKey).to.deep.equal('1133')
  })
})
