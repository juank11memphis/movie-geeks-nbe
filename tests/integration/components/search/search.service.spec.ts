import { expect } from 'chai'
import sinon from 'sinon'

import { testsSetup } from '../../tests.util'
import { SearchService } from '../../../../src/components/search/search.service'
import { TheMovieDBApi } from '../../../../src/shared/api/themoviedb'
import { expectedSearchResult1 } from '../../snapshots/search'
import { themoviedbApiParsedSearchResponse } from '../../mock-data/themoviedb.mocks'

const sandbox = sinon.createSandbox()
const searchService = new SearchService()

describe('SearchService', () => {
  beforeEach((done: any) => {
    void testsSetup(done)
  })

  it('should search movies', async () => {
    sandbox
      .stub(TheMovieDBApi.prototype, 'searchMovies')
      .returns(Promise.resolve(themoviedbApiParsedSearchResponse))
    const searchResult = await searchService.search('fight')
    expect(searchResult).to.containSubset(expectedSearchResult1)
    sandbox.restore()
  })

  it('should handle search when query is null or empty string', async () => {
    let searchResult = await searchService.search(undefined)
    expect(searchResult).to.deep.equal({ movies: [], page: 0, totalPages: 0, totalResults: 0 })
    searchResult = await searchService.search('')
    expect(searchResult).to.deep.equal({ movies: [], page: 0, totalPages: 0, totalResults: 0 })
  })
})
