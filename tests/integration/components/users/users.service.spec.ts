import { expect } from 'chai'
import mongodb from 'mongodb'

import { testsSetup } from '../../tests.util'
import { UsersService } from '../../../../src/components/users/users.service'
import { ids } from '../../fixtures-ids'
import { expectedJuanUser, expectedJuanUpdated } from '../../snapshots/users'

const usersService = new UsersService()

describe('UsersService', () => {
  beforeEach((done: any) => {
    void testsSetup(done)
  })

  it('should load users by ids', async () => {
    const { users } = ids
    const songsLoaded = await usersService.loadByIds([users.juanId, users.sanId])
    expect(songsLoaded.length).to.equal(2)
  })

  it('should create an user', async () => {
    const user = await usersService.create({
      firstname: 'Juanca',
      lastname: 'Morales',
      email: 'juan2@gmail.com',
    })
    expect(user._id).not.to.be.undefined
    expect(user.firstname).to.equal('Juanca')
    expect(user.email).to.equal('juan2@gmail.com')
  })

  it('Should load user details by email', async () => {
    const { users } = ids
    const user = await usersService.getDetailByEmail('juan@gmail.com')
    expect(user._id + '').to.deep.equal(users.juanId + '')
    expect(user.firstname).to.equal('Juan')
  })

  it('should load user details', async () => {
    const { users } = ids
    const user = await usersService.getDetailById(users.juanId)
    expect(user).to.containSubset(expectedJuanUser)
  })

  it('should update user', async () => {
    const { users } = ids
    const user = await usersService.updateUser(users.juanId, {
      firstname: 'Carlos',
      lastname: 'Morales',
      email: 'juan@gmail.com',
    })
    expect(user).to.containSubset(expectedJuanUpdated)
    expect(user.updatedAt).not.to.be.undefined
  })

  it('should throw exception if trying to update a user that does not exists in database', async () => {
    const badUserId = new mongodb.ObjectID() + ''
    try {
      await usersService.updateUser(badUserId, { email: '' })
    } catch (error) {
      expect(error.message).to.deep.equal(`User with id ${badUserId} not found in the database`)
    }
    try {
      await usersService.rawUpdateUser(badUserId, { email: '' })
    } catch (error) {
      expect(error.message).to.deep.equal(`User with id ${badUserId} not found in the database`)
    }
  })
})
