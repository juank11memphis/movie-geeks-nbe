import { expect } from 'chai'

import { recalculateRatingsData } from '../../../src/shared/util/ratings'

describe('Ratings Util', () => {
  afterEach(() => {
    process.env.NODE_ENV = 'test'
  })

  it('should recalculate ratings data', () => {
    const data = recalculateRatingsData({ title: 'Seven' })
    expect(data).to.deep.equal({ ratingsSum: 0, ratingsCount: 1, globalRating: 0 })
  })
})
