import { expect } from 'chai'
import sinon from 'sinon'
import request from 'request'
import { get } from 'lodash'

import { doRequest } from '../../../src/shared/util/request'

describe('Request Util', () => {
  afterEach(() => {
    const stubGet: any = get(request, 'get')
    stubGet.restore()
  })

  it('should do a successful request', async () => {
    sinon.stub(request, 'get').yields(undefined, { statusCode: 200 }, 'this is google')
    const result = await doRequest({ url: 'https://www.google.com/' })
    expect(result).to.equal('this is google')
  })

  it('should return a resource not found error', async () => {
    sinon.stub(request, 'get').yields(undefined, { statusCode: 404 }, undefined)
    try {
      await doRequest({ url: 'https://www.google.com/' })
    } catch (error) {
      expect(error.message).to.equal('Resource not found')
    }
  })

  it('should return generic request error', async () => {
    sinon.stub(request, 'get').yields(new Error('Bad request'), { statusCode: 400 }, undefined)
    try {
      await doRequest({ url: 'https://www.google.com/' })
    } catch (error) {
      expect(error.message).to.equal('Bad request')
    }
  })

  it('should return unknown error', async () => {
    sinon.stub(request, 'get').yields(undefined, { statusCode: 400 }, undefined)
    try {
      await doRequest({ url: 'https://www.google.com/' })
    } catch (error) {
      expect(error.message).to.equal('Unknown error on request: https://www.google.com/')
    }
  })
})
