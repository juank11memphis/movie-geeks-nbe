import { expect } from 'chai'

import { UserModel } from '../../../src/components/users/user'
import { removeMany, updateMany } from '../../../src/shared/util/db'
import { testsSetup } from '../tests.util'
import { ids } from '../fixtures-ids'

describe('DB util', () => {
  beforeEach((done: any) => {
    void testsSetup(done)
  })

  it('should handle updateMany when empty array is sent', async () => {
    const result = await updateMany(UserModel)
    expect(result).to.deep.equal([])
  })

  it('should update many records at once', async () => {
    const { users } = ids
    const juanUser = {
      id: users.juanId,
      data: { lastname: 'Morales2' },
    }
    const sanUser = {
      id: users.sanId,
      data: { lastname: 'Aguilar2' },
    }
    await updateMany(UserModel, [juanUser, sanUser])
    const result = await UserModel.find({
      _id: { $in: [users.juanId, users.sanId] },
    })
    expect(result).to.containSubset([
      { _id: users.juanId, lastname: 'Morales2' },
      { _id: users.sanId, lastname: 'Aguilar2' },
    ])
  })

  it('should handle removeMany when empty array is sent', async () => {
    const result = await removeMany(UserModel)
    expect(result).to.deep.equal([])
  })

  it('Should remove many successfully', async () => {
    const { users } = ids
    await removeMany(UserModel, [users.juanId, users.sanId, users.joseId])
    const dbUsers = await UserModel.find({})
    expect(dbUsers.length).to.equal(27)
  })
})
