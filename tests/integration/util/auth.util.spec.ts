// 8,53,74,78

import { expect } from 'chai'

import {
  generateToken,
  validateToken,
  getTokenData,
  validatePermissions,
  hashPassword,
  comparePasswords,
} from '../../../src/shared'
import { DecodedToken } from '../../../src/types'
import { testsSetup } from '../tests.util'

const testUser = {
  _id: '1',
  firstname: 'Juan Carlos',
  lastname: 'Morales',
  email: 'juan@gmail.com',
  roles: [
    {
      _id: '1',
      name: 'admins',
      permissions: ['create:quote', 'update:footer'],
    },
    {
      _id: '2',
      name: 'Movie Geeks',
      permissions: ['update:movie', 'update:artist'],
    },
  ],
}

const testUser2 = {
  _id: '1',
  name: 'Sandra',
  roles: [
    {
      _id: '1',
      name: 'admins',
      permissions: ['create:quote', 'update:footer'],
    },
    {
      _id: '2',
      name: 'Designer',
      permissions: ['update:styles'],
    },
  ],
}

describe('AuthUtil', () => {
  beforeEach((done: any) => {
    void testsSetup(done)
  })

  it('should decode valid tokens', async () => {
    const token = await generateToken(testUser._id, testUser.roles)
    const decodedToken = await validateToken(`bearer ${token}`)
    expect(decodedToken.userId).to.equal(testUser._id)
    expect(decodedToken.userRoles).to.containSubset(testUser.roles)
  })

  it('should decode valid tokens when passing no userRoles', async () => {
    const token = await generateToken(testUser._id)
    expect(token).not.to.be.undefined
  })

  it('should throw error if token is not provided', async () => {
    try {
      await validateToken()
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw error if token is not a bearer token', async () => {
    try {
      await validateToken('not a bearer token')
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw error if an invalid token is provided', async () => {
    try {
      await validateToken('bearer a bad token')
    } catch (error) {
      expect(error.message).to.equal('Unauthorized!!')
    }
  })

  it('should throw not enough permissions error if user has no roles', () => {
    try {
      validatePermissions('update:movie')
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
    try {
      validatePermissions('update:movie', [])
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should throw not enough permissions error if user roles have invalid permissions', () => {
    try {
      validatePermissions('update:movie', [{ name: 'test', permissions: undefined }])
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
    try {
      validatePermissions('update:movie', [{ name: 'test', permissions: [] }])
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should throw not enough permissions error if user dont have a specific permission', () => {
    try {
      validatePermissions('update:movie', testUser2.roles)
    } catch (error) {
      expect(error.message).to.equal('Not enough permissions')
    }
  })

  it('should return true if user has a specific permission', () => {
    const hasPermissions = validatePermissions('update:movie', testUser.roles)
    expect(hasPermissions).to.equal(true)
  })

  it('should return null if invalid token is provided', async () => {
    const decodedToken = await getTokenData('bearer a bad token')
    expect(decodedToken).to.equal(undefined)
  })

  it('should return tokenData if valid token is provided', async () => {
    const token = await generateToken(testUser._id, testUser.roles)
    const decodedToken = (await getTokenData(`bearer ${token}`)) as DecodedToken
    expect(decodedToken.userId).to.equal(testUser._id)
    expect(decodedToken.userRoles).to.containSubset(testUser.roles)
  })

  it('should hash a password', () => {
    expect(comparePasswords('password', hashPassword('password'))).to.equal(true)
  })

  it('should hash a password when not passing the encrypted password', () => {
    expect(comparePasswords('password')).to.equal(false)
  })

  it('should hash a password when not passing a valid password', () => {
    const hash = hashPassword()
    expect(hash).not.to.be.undefined
  })
})
