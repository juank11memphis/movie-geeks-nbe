import mongoose, { Mongoose } from 'mongoose'

import { getValue } from '../shared'

// Use native promises
mongoose.Promise = global.Promise

const mongoUrl = getValue('mongodb_url')
const mongoUsername = getValue('mongodb_username')
const mongoPassword = getValue('mongodb_password')

export const initConnection = async (): Promise<Mongoose> => {
  return mongoose.connect(mongoUrl, {
    socketTimeoutMS: 10000,
    useNewUrlParser: true,
    user: mongoUsername,
    pass: mongoPassword,
  })
}
