import {
  discoverQueryTypes,
  moviesQueryTypes,
  searchQueryTypes,
  usersQueryTypes,
  userMoviesQueryTypes,
} from '../../components'

export const queryTypes = `
  type Query {
    ${discoverQueryTypes}
    ${moviesQueryTypes}
    ${searchQueryTypes}
    ${usersQueryTypes}
    ${userMoviesQueryTypes}
  }
`
