import { queryTypes } from './queries'
import { mutationsTypes } from './mutations'
import { sharedTypes } from './shared-graph-types'
import {
  authType,
  userType,
  userRoleType,
  discoverType,
  movieType,
  userMovieType,
  searchType,
} from '../../components'

export const typeDefs = `
  ${sharedTypes}
  ${queryTypes}
  ${mutationsTypes}
  ${authType}
  ${discoverType}
  ${movieType}
  ${searchType}
  ${userMovieType}
  ${userType}
  ${userRoleType}
`

// export default `
//   ${queryTypes}
//   ${mutationsTypes}
//   ${sharedTypes}
// `
