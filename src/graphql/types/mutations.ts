import { authMutationTypes, userMoviesMutationTypes } from '../../components'

export const mutationsTypes = `
  type Mutation {
    ${authMutationTypes}
    ${userMoviesMutationTypes}
  }
`
