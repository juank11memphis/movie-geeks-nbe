import DataLoader from 'dataloader'

import { AppService } from '../types'

export const createDataLoader = (serviceInstance: AppService): DataLoader<string, any, any[]> => {
  const findItems = (ids: ReadonlyArray<string>): Promise<any[]> => {
    const idsArray = ids as string[]
    return serviceInstance.loadByIds(idsArray)
  }
  return new DataLoader<string, any[]>(findItems)
}
