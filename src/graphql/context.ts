import {
  UsersService,
  AuthService,
  UserRolesService,
  SearchService,
  MoviesService,
  UserMoviesService,
  DiscoverService,
} from '../components'
import { createDataLoader } from './loader.factory'
import { getTokenData } from '../shared'
import { ServerContextParams, AppContext } from '../types'

export const context = async ({ event }: ServerContextParams): Promise<AppContext> => {
  let user
  let resetPasswordUser
  const { headers } = event
  const token = headers['authorization'] || headers['Authorization']
  const resetPasswordToken = headers['reset-password-token']
  const tokenData = await getTokenData(token || '')
  const resetPasswordTokenData = await getTokenData(
    resetPasswordToken || '',
    'reset_password_token_secret',
  )
  const usersService = new UsersService()
  const moviesService = new MoviesService()
  if (tokenData) {
    user = await usersService.getDetailById(tokenData.userId)
  }
  if (resetPasswordTokenData) {
    resetPasswordUser = await usersService.getDetailById(resetPasswordTokenData.userId)
  }
  return {
    token,
    currentUser: user,
    resetPasswordUser,
    tokenData,
    usersService,
    authService: new AuthService(),
    searchService: new SearchService(),
    moviesService,
    userMoviesService: new UserMoviesService(),
    discoverService: new DiscoverService(),
    loaders: {
      users: createDataLoader(usersService),
      userRoles: createDataLoader(new UserRolesService()),
      movies: createDataLoader(moviesService),
    },
  }
}
