import {
  authMutationsResolvers,
  discoverQueriesResolvers,
  discoverItemTypeResolvers,
  moviesQueriesResolvers,
  userMovieTypeResolver,
  searchQueriesResolvers,
  userQueriesResolvers,
  userMoviesQueryResolvers,
  userMoviesMutationsResolvers,
} from '../components'

export const resolvers = {
  Query: {
    ...discoverQueriesResolvers,
    ...moviesQueriesResolvers,
    ...searchQueriesResolvers,
    ...userQueriesResolvers,
    ...userMoviesQueryResolvers,
  },
  DiscoverItem: discoverItemTypeResolvers,
  UserMovie: userMovieTypeResolver,
  Mutation: {
    ...authMutationsResolvers,
    ...userMoviesMutationsResolvers,
  },
}
