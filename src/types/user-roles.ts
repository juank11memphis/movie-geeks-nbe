import { Document } from 'mongoose'

interface BaseUserRole {
  name: string
  permissions?: string[]
}

export interface DBUserRole extends Document, BaseUserRole {}

export interface UserRole extends BaseUserRole {
  id?: string
}
