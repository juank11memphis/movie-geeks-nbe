import { Document } from 'mongoose'

export interface MovieRatingsData {
  globalRating: number
  ratingsCount: number
  ratingsSum: number
}

interface BaseMovie {
  originalTitle?: string
  posterPath?: string
  genres?: string[]
  overview?: string
  releaseDate?: string
  year?: string
  tmdbId?: string
  ratingsData?: MovieRatingsData
  youtubeTrailerKey?: string
}

export interface DBMovie extends Document, BaseMovie {}

export interface Movie extends BaseMovie {
  id?: string
  title?: string
}

export interface MovieFilters {
  genres?: string[]
  minimumRating?: number
  yearsRange?: number[]
}

export interface GetYoutubeOfficialTrailerKeyQueryArgs {
  movieId: string
}
