import { Document } from 'mongoose'

export interface UserUsersMovies {
  id: string
  points: number
}

export interface UserUsers {
  user?: string
  comparandUser?: string
  score: number
  movies: UserUsersMovies[]
}

export interface DBUserUsers extends Document, UserUsers {}

export interface SimilarUsersData {
  data: DBUserUsers[]
  moviesInCommonIds: string[]
  userIds: string[]
}
