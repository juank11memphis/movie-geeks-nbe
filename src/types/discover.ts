import { Movie } from './movies'

export interface DiscoverItem {
  id: string
  movieId: string
  averageRating: number
  totalRating: number
  count: number
  movie: Movie
  users: string[]
}

export interface DiscoverResponse {
  items: DiscoverItem[]
  users: string[]
}
