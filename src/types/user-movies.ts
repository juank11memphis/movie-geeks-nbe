import { Document } from 'mongoose'

import { User } from './users'
import { Movie } from './movies'
import { PagingMetadata, PagingAndFilters } from './general'

interface BaseUserMovie {
  user: string
  movie: string
  rating?: number
  saved?: boolean
  comparandUser?: ComparandUser
  userDetail?: User
  movieDetail?: Movie
}

export interface UserMovieUpdateData {
  rating?: number
  saved?: boolean
}

export interface DBUserMovie extends Document, BaseUserMovie {}

export interface UserMovie extends BaseUserMovie {
  id?: string
}

export interface ComparandUser {
  userId: string
  rating?: number
  saved?: boolean
}

export interface UserMoviesPaginated {
  userMovies: UserMovie[]
  pagingMetadata?: PagingMetadata
}

export interface UserMoviesQueryArgs {
  mainUserId?: string
  comparandUserId?: string
  impersonateUserId?: string
  params: PagingAndFilters
}

export interface UserMoviesMutationArgs {
  movieId: string
  rating: number
}
