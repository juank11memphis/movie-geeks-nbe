import { DBMovie, Movie } from './movies'
import { UserMovie } from './user-movies'

interface BaseSearchResponse {
  page: number
  totalResults: number
  totalPages: number
}

export interface SearchResponse extends BaseSearchResponse {
  movies: DBMovie[]
}

export interface TheMovieDBSearchResponse extends BaseSearchResponse {
  movies: Movie[]
}

export interface UserMoviesSearchResponse extends SearchResponse {
  userMovies: UserMovie[]
}

export interface SearchQueryArgs {
  query: string
}
