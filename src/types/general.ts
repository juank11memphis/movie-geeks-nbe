import { MovieFilters } from './movies'

export interface AppService {
  loadByIds: (ids: string[]) => Promise<any[]>
}

export interface MongoBulkUpdateOne {
  updateOne: {
    filter: { _id: string }
    update: { $set: {} }
  }
}

export interface RequestOptions {
  url: string
  json: boolean
}

export interface PagingMetadata {
  previous?: string
  hasPrevious?: boolean
  next?: string
  hasNext?: boolean
}

export interface PagingParams {
  next?: string
  previous?: string
  limit?: number
}

export interface PagingAndFilters {
  filters?: MovieFilters
  paging?: PagingParams
}
