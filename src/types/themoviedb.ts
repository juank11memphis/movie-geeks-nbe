export interface TheMovieDBMovie {
  id: string
  title: string
  original_title: string
  poster_path?: string
  genre_ids?: number[]
  overview?: string
  release_date?: string
}

interface TheMovieDBVideo {
  name: string
  key: string
}

interface TheMovieDBBaseResponse {
  page: number
  total_results: number
  total_pages: number
}

export interface TheMovieDBResponse extends TheMovieDBBaseResponse {
  results: TheMovieDBMovie[]
}

export interface TheMovieDBVideosResponse extends TheMovieDBBaseResponse {
  results: TheMovieDBVideo[]
}
