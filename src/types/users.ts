import { Document } from 'mongoose'
import { UserRole } from './user-roles'

interface BaseUser {
  firstname?: string
  lastname?: string
  email: string
  password?: string
  hasPassword?: boolean
  updatedAt?: string
  roles?: UserRole[]
}

export interface DBUser extends Document, BaseUser {}

export interface User extends BaseUser {
  id?: string
}

export interface UserUpdateData {
  firstname?: string
  lastname?: string
  email?: string
  password?: string
}

export interface UserGetOrCreateResponse {
  isNewUser: boolean
  dbUser: DBUser
}

export interface LoadUserByIdArgs {
  userId: string
}
