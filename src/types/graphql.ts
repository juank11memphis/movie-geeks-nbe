import DataLoader from 'dataloader'

import { DBUser } from './users'
import { DecodedToken } from './auth'
import {
  UsersService,
  AuthService,
  SearchService,
  MoviesService,
  UserMoviesService,
  DiscoverService,
} from '../components'
import { DBUserRole } from './user-roles'
import { DBMovie } from './movies'

export interface ServerContextEvent {
  headers: {
    authorization?: string
    Authorization?: string
    'reset-password-token'?: string
  }
}

export interface ServerContextParams {
  event: ServerContextEvent
}

export interface AppContextLoaders {
  users: DataLoader<string, DBUser, DBUser[]>
  userRoles: DataLoader<string, DBUserRole, DBUserRole[]>
  movies: DataLoader<string, DBMovie, DBMovie[]>
}

export interface AppContext {
  token?: string
  currentUser?: DBUser
  resetPasswordUser?: DBUser
  tokenData?: DecodedToken
  usersService: UsersService
  authService: AuthService
  searchService: SearchService
  moviesService: MoviesService
  userMoviesService: UserMoviesService
  discoverService: DiscoverService
  loaders: AppContextLoaders
}
