import { DBUser, User } from './users'

export interface DecodedToken {
  userId: string
  userRoles?: string[]
}

export interface AuthResponse {
  user: DBUser
  token: string
}

export interface SigninMutationArgs {
  email: string
  password: string
}

export interface SignupMutationArgs {
  user: User
}

export interface RequestPasswordResetMutationArgs {
  email: string
}

export interface ResetPasswordMutationArgs {
  password: string
}
