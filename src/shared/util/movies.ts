import { get } from 'lodash'

const baseUserMovieStages = [
  {
    $lookup: {
      from: 'movies',
      localField: 'movie',
      foreignField: '_id',
      as: 'movieDetail',
    },
  },
  { $unwind: '$movieDetail' },
  { $addFields: { yearInt: { $toInt: '$movieDetail.year' } } },
]

const getGenreFilters = (filters: any): {} => {
  const genresFilters = get(filters, 'genres')
  return genresFilters
    ? {
        'movieDetail.genres': { $in: genresFilters },
      }
    : {}
}

const getYearsRangeFilters = (filters: any): {} => {
  const yearsRangeFilters = get(filters, 'yearsRange')
  return yearsRangeFilters
    ? {
        yearInt: {
          $gte: parseInt(yearsRangeFilters[0]),
          $lte: parseInt(yearsRangeFilters[1]),
        },
      }
    : {}
}

const getMinimumRatingFilter = (filters: any): {} => {
  const minimumRatingFilter = get(filters, 'minimumRating')
  return minimumRatingFilter
    ? {
        rating: { $gte: parseInt(minimumRatingFilter) },
      }
    : {}
}

const getMatchStage = (baseMatchQuery: any, filters: any): {} => ({
  $match: {
    ...baseMatchQuery,
    ...getGenreFilters(filters),
    ...getYearsRangeFilters(filters),
    ...getMinimumRatingFilter(filters),
  },
})

export const constructUserMovieAggregate = ({ baseMatchQuery, filters }: any): {}[] => {
  return [...baseUserMovieStages, getMatchStage(baseMatchQuery, filters)]
}
