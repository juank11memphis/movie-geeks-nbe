import request from 'request'
import { get } from 'lodash'

export function doRequest<T>(optionsOrUrl: request.UrlOptions, method = 'get'): Promise<T> {
  return new Promise((resolve: any, reject: any) => {
    const methodFunc = get(request, method)
    methodFunc(optionsOrUrl, (err: any, res: request.Response, body: any) => {
      if (!err && res && (res.statusCode === 200 || res.statusCode === 204)) {
        resolve(body)
      } else if (res && res.statusCode === 404) {
        reject(new Error('Resource not found'))
      } else {
        const url = optionsOrUrl.url
        reject(err || new Error(`Unknown error on request: ${url}`))
      }
    })
  })
}
