import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { AuthenticationError } from 'apollo-server-lambda'

import { getValue, getValueAsInt } from './config'
import { UserRole, DecodedToken } from '../../types'

export const generateToken = (userId: string, roles: UserRole[] = []): string => {
  return jwt.sign({ id: userId, roles }, getValue('token_secret'), {
    expiresIn: getValue('token_expiration'),
  })
}

export const generateResetPasswordToken = (userId: string): string => {
  return jwt.sign({ id: userId }, getValue('reset_password_token_secret'), {
    expiresIn: getValue('reset_password_token_expiration'),
  })
}

export const validateToken = async (
  authorization = '',
  tokenSecretKey = 'token_secret',
): Promise<DecodedToken> => {
  const [jwtType, token] = authorization.split(' ')
  if (jwtType.toLowerCase() !== 'bearer' || !token) {
    throw new AuthenticationError('Unauthorized!!')
  }
  let decoded: any
  try {
    decoded = await jwt.verify(token, getValue(tokenSecretKey))
  } catch (error) {
    throw new AuthenticationError('Unauthorized!!')
  }
  return {
    userId: decoded.id,
    userRoles: decoded.roles,
  }
}

export const getTokenData = async (
  token: string,
  tokenSecretKey = 'token_secret',
): Promise<DecodedToken | undefined> => {
  let tokenData
  try {
    tokenData = await validateToken(token, tokenSecretKey)
  } catch (error) {
    tokenData = undefined
  }
  return tokenData
}

export const validatePermissions = (permissionToFind: string, roles: UserRole[] = []): boolean => {
  if (roles.length === 0) {
    throw new AuthenticationError('Not enough permissions')
  }
  let permissionFound = false
  roles.forEach(({ permissions }: UserRole) => {
    if (!permissions) {
      return
    }
    permissions.forEach((permission: string) => {
      if (permission === permissionToFind) {
        permissionFound = true
      }
    })
  })
  if (!permissionFound) {
    throw new AuthenticationError('Not enough permissions')
  }
  return true
}

export const comparePasswords = (password: string, encryptedPassword = ''): boolean => {
  return bcrypt.compareSync(password, encryptedPassword)
}

export const hashPassword = (password = ''): string => {
  return bcrypt.hashSync(password, getValueAsInt('security_salt_rounds'))
}
