import { get } from 'lodash'
import { Movie, MovieRatingsData } from '../../types'

export const recalculateRatingsData = (
  item: Movie,
  currentUserRating = 0,
  newUserRating = 0,
): MovieRatingsData => {
  const isNewRating = !currentUserRating ? true : false
  const finalCurrentRating = isNewRating ? 0 : currentUserRating

  const { ratingsCount, ratingsSum } = get(item, 'ratingsData', {
    globalRating: 0,
    ratingsCount: 0,
    ratingsSum: 0,
  })
  const newRatingsSum = ratingsSum - finalCurrentRating + newUserRating
  const newRatingsCount = isNewRating ? ratingsCount + 1 : ratingsCount
  return {
    ratingsSum: newRatingsSum,
    ratingsCount: newRatingsCount,
    globalRating: newRatingsSum / newRatingsCount,
  }
}

export const recalculateRatingsDataOnRemoveRating = (
  item: Movie,
  currentRating: number,
): MovieRatingsData => {
  const { ratingsCount, ratingsSum } = get(item, 'ratingsData', {
    globalRating: 0,
    ratingsCount: 0,
    ratingsSum: 0,
  })
  const newRatingsSum = ratingsSum - currentRating
  const newRatingsCount = ratingsCount - 1
  return {
    ratingsSum: newRatingsSum,
    ratingsCount: newRatingsCount,
    globalRating: newRatingsSum / newRatingsCount,
  }
}

export const getValidRatingValue = (rating: number): number => {
  // we dont take ratings lower than 0
  if (rating < 0) {
    return 0
  }
  // we dont take ratings greater than 10
  if (rating > 10) {
    return 10
  }
  return rating
}
