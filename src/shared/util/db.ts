import { Model } from 'mongoose'
import { BulkWriteOpResultObject } from 'mongodb'

import { MongoBulkUpdateOne } from '../../types'

export const isValidDBId = (id: string): boolean => {
  const stringId = id + ''
  const matches = stringId.match(/^[0-9a-fA-F]{24}$/)
  if (matches) {
    return true
  }
  return false
}

export const getBulkUpdateObject = (id: string, data: any): MongoBulkUpdateOne => ({
  updateOne: {
    filter: { _id: id },
    update: { $set: { ...data } },
  },
})

export const updateMany = async (
  modelClass: Model<any>,
  updates: {}[] = [],
): Promise<BulkWriteOpResultObject | []> => {
  if (updates.length === 0) {
    return []
  }
  const bulkUpdates: MongoBulkUpdateOne[] = []
  updates.forEach(({ id, data }: any) => {
    bulkUpdates.push(getBulkUpdateObject(id, data))
  })
  return modelClass.collection.bulkWrite(bulkUpdates, { ordered: true, w: 1 })
}

export const removeMany = async (modelClass: Model<any>, deletes: {}[] = []): Promise<any | []> => {
  if (deletes.length === 0) {
    return []
  }
  return modelClass.deleteMany({ _id: { $in: deletes } })
}
