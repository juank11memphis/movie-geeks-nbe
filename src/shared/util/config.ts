import { get } from 'lodash'

/* eslint-disable @typescript-eslint/camelcase */
const defaults = {
  environment: 'local',
  test: {
    mongodb_url: 'mongodb://localhost:27017/test?authSource=admin',
    mongo_username: 'juanca',
    mongo_password: 'password',
    redis_url: 'redis://redis:6379',
    port: 5678,
    token_secret: 'a_test_secret',
    reset_password_token_secret: 'another_test_secret',
    token_expiration: '1d',
    reset_password_token_expiration: '1d',
    security_salt_rounds: 4,
    themoviedb_key: 'a_movide_db_key',
    mailgun_apikey: 'an_awesome_key',
    mailgun_domain: 'awesome@domain.com',
    support_email: 'support@moviegeeks.co',
    reset_password_email_link: 'http://localhost:3000/reset-password',
  },
}

export const getEnvironment = (): string => {
  if (!process.env.NODE_ENV) {
    return defaults.environment
  }
  return process.env.NODE_ENV
}

const isTest = (): boolean => {
  const environment = getEnvironment()
  return environment.includes('test')
}

export const getValue = (key: string, defaultValue?: any): any => {
  const testsDefaultValue = get(defaults.test, key)
  if (isTest() && testsDefaultValue) {
    return testsDefaultValue
  }
  return process.env[key] || defaultValue
}

export const getValueAsInt = (key: string): number => {
  const valueStr = getValue(key)
  return parseInt(valueStr)
}

export const getVersion = (): string => {
  return require('../../../package.json').version
}

export const isProd = (): boolean => {
  const environment = getEnvironment()
  return environment === 'production'
}
