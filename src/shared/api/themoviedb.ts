import { get } from 'lodash'

import { doRequest } from '../util/request'
import { genresMap } from '../constants'
import { getValue } from '../util/config'
import { generalLogger } from '../logs/general.logger'
import {
  TheMovieDBResponse,
  TheMovieDBVideosResponse,
  TheMovieDBMovie,
  Movie,
  RequestOptions,
  TheMovieDBSearchResponse,
} from '../../types'

export class TheMovieDBApi {
  BASE_URL = 'https://api.themoviedb.org/3'

  async getPlayingNow(): Promise<Movie[]> {
    generalLogger.info(`Requesting TheMovieDB Api getPlayingNow endpoint`)
    console.time('themoviedbApi.getPlayingNow')
    const url = this.getUrl('movie/now_playing?region=US')
    const response = await doRequest<TheMovieDBResponse>(this.getDefaultRequestOptions(url))
    const parsedMovies = this.parseMovies(response.results)
    console.timeEnd('themoviedbApi.getPlayingNow')
    return parsedMovies
  }

  async searchMovies(query: string): Promise<TheMovieDBSearchResponse> {
    console.time('themoviedbApi.searchMovies')
    const queryEncoded = encodeURIComponent(query)
    generalLogger.info(`Requesting TheMovieDB Api Search endpoint with query: ${queryEncoded}`)
    const url = this.getUrl(`search/movie?query=${queryEncoded}`)
    const response = await doRequest<TheMovieDBResponse>(this.getDefaultRequestOptions(url))
    const parsedResponse = this.parseSearchResponse(response)
    console.timeEnd('themoviedbApi.searchMovies')
    return parsedResponse
  }

  parseSearchResponse(response: TheMovieDBResponse): TheMovieDBSearchResponse {
    return {
      page: response.page,
      totalResults: response.total_results,
      totalPages: response.total_pages,
      movies: this.parseMovies(response.results),
    }
  }

  parseMovies(movies: TheMovieDBMovie[]): Movie[] {
    return movies.map((apiMovie: TheMovieDBMovie) => ({
      id: apiMovie.id,
      title: apiMovie.title,
      originalTitle: apiMovie.original_title,
      posterPath: apiMovie.poster_path ? apiMovie.poster_path : undefined,
      genres: this.parseMovieGenres(apiMovie.genre_ids),
      overview: apiMovie.overview,
      releaseDate: apiMovie.release_date,
      year: this.extractYearFromReleaseDate(apiMovie.release_date),
    }))
  }

  parseMovieGenres(genreIds: number[] = []): string[] | [] {
    return genreIds && genreIds.length > 0
      ? genreIds.map((genreId: number) => genresMap[genreId])
      : []
  }

  extractYearFromReleaseDate(releaseDate?: string): string {
    return releaseDate ? releaseDate.substring(0, releaseDate.indexOf('-')) : ''
  }

  getUrl(path: string): string {
    const apiKey = getValue('themoviedb_key')
    if (path.indexOf('?') >= 0) {
      return `${this.BASE_URL}/${path}&api_key=${apiKey}`
    } else {
      return `${this.BASE_URL}/${path}?api_key=${apiKey}`
    }
  }

  getDefaultRequestOptions(url: string): RequestOptions {
    return {
      url,
      json: true,
    }
  }

  async getOfficialTrailerYoutubeKey(tmdbId = ''): Promise<string> {
    generalLogger.info(`Requesting TheMovieDB Api movie videos endpoint`)
    console.time('themoviedbApi.getOfficialTrailerYoutubeKey')
    const url = this.getUrl(`movie/${tmdbId}/videos`)
    const response = await doRequest<TheMovieDBVideosResponse>(this.getDefaultRequestOptions(url))
    const results = get(response, 'results', [])
    const officialTrailer = results.find(video => {
      const videoName = video.name.toLowerCase()
      return videoName.indexOf('official trailer') > -1
    })
    const finalTrailer = officialTrailer || results[0]
    const officalTrailerKey = get(finalTrailer, 'key')
    console.timeEnd('themoviedbApi.getOfficialTrailerYoutubeKey')
    return officalTrailerKey
  }
}
