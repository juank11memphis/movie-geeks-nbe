import { keyBy } from 'lodash'
import { ApolloError } from 'apollo-server-lambda'

import { UsersDao } from './users.dao'
import { User, DBUser, UserGetOrCreateResponse, UserUpdateData } from '../../types'

export class UsersService {
  usersDao: UsersDao

  constructor() {
    this.usersDao = new UsersDao()
  }

  async loadByIds(ids: string[]): Promise<DBUser[]> {
    const users = await this.usersDao.loadByIds(ids)
    const usersById = keyBy(users, '_id')
    const usersSorted = ids.map((movieId: string) => usersById[movieId])
    return usersSorted
  }

  create(data: User): Promise<DBUser> {
    return this.usersDao.create(data)
  }

  updateUser = async (userId: string, updateData: UserUpdateData): Promise<DBUser> => {
    delete updateData.password
    delete updateData.email
    const dbUser = await this.usersDao.getDetailById(userId)
    if (!dbUser) {
      throw new ApolloError(`User with id ${userId} not found in the database`)
    }
    await this.usersDao.update(dbUser, updateData)
    return this.getDetailById(userId)
  }

  async rawUpdateUser(userId: string, updateData: UserUpdateData): Promise<DBUser> {
    const dbUser = await this.usersDao.getDetailById(userId)
    if (!dbUser) {
      throw new ApolloError(`User with id ${userId} not found in the database`)
    }
    return this.usersDao.update(dbUser, updateData)
  }

  async getDetailByEmail(email: string): Promise<DBUser> {
    const user = await this.usersDao.getDetailByEmail(email)
    if (!user) {
      throw new ApolloError(`User with email ${email} not found in the database`)
    }
    return user
  }

  async getDetailById(userId: string): Promise<DBUser> {
    const user = await this.usersDao.getDetailById(userId)
    if (!user) {
      throw new ApolloError(`User with id ${userId} not found in the database`)
    }
    return user
  }

  async getByEmailOrCreate(email: string, userData: User): Promise<UserGetOrCreateResponse> {
    try {
      const dbUser = await this.getDetailByEmail(email)
      return {
        isNewUser: false,
        dbUser,
      }
    } catch (error) {
      const dbUser = await this.create(userData)
      return {
        isNewUser: true,
        dbUser,
      }
    }
  }
}
