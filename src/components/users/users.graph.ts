import { AuthenticationError } from 'apollo-server-lambda'
import { DBUser, AppContext, LoadUserByIdArgs } from '../../types'

export const userType = `
  type User {
    id: String!
    firstname: String
    lastname: String
    email: String
    hasPassword: Boolean
  }

  type UserShort {
    id: String!
    firstname: String
    lastname: String
  }

  input UserInput {
    firstname: String
    lastname: String
    email: String
    password: String
  }
`

export const usersQueryTypes = `
  loadAuthUser: User
  loadUserById(userId: String): UserShort
`

export const userQueriesResolvers = {
  loadAuthUser: (
    root: {},
    args: {},
    { usersService, currentUser }: AppContext,
  ): Promise<DBUser> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return usersService.getDetailById(currentUser._id)
  },
  loadUserById: (
    root: {},
    { userId }: LoadUserByIdArgs,
    { usersService }: AppContext,
  ): Promise<DBUser> => usersService.getDetailById(userId),
}
