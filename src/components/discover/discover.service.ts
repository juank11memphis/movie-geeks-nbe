import { UserUsersService } from '../user-users/user-users.service'
import { UserMoviesService } from '../user-movies/user-movies.service'
import { DiscoverDao } from './discover.dao'
import { DiscoverResponse } from '../../types'

export class DiscoverService {
  userUsersService: UserUsersService
  userMoviesService: UserMoviesService
  discoverDao: DiscoverDao

  constructor() {
    this.userUsersService = new UserUsersService()
    this.userMoviesService = new UserMoviesService()
    this.discoverDao = new DiscoverDao()
  }

  discover = async (userId: string): Promise<DiscoverResponse> => {
    const topUserUsers = await this.userUsersService.getDetailedTopSimilarUsers(userId, 10)

    // we need at least five movies in common to return accurate results
    if (topUserUsers.moviesInCommonIds.length < 5) {
      return { users: [], items: [] }
    }

    const userFullCollection = await this.userMoviesService.getFullCollection(userId)
    const userFullCollectionMovieIds = userFullCollection.map(userMovie => userMovie.movie + '')

    const userFullWatchlist = await this.userMoviesService.getFullWatchlist(userId)
    const userFullWatchlistMovieIds = userFullWatchlist.map(userMovie => userMovie.movie + '')

    const discoverResult = await this.discoverDao.discover(topUserUsers.userIds, [
      ...userFullCollectionMovieIds,
      ...userFullWatchlistMovieIds,
    ])

    return {
      users: topUserUsers.userIds,
      items: discoverResult,
    }
  }
}
