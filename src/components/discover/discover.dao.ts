import mongodb from 'mongodb'
import mongoose from 'mongoose'
import { DiscoverItem } from '../../types'

export class DiscoverDao {
  async discover(userIds: string[], excludeMovieIds: string[]): Promise<DiscoverItem[]> {
    const userIdsForAggr = userIds.map((userId: string) => new mongodb.ObjectID(userId))
    const movieIdsForAggr = excludeMovieIds.map((userId: string) => new mongodb.ObjectID(userId))
    const discoverItems = await mongoose.connection.db
      .collection('user-movies')
      .aggregate([
        {
          $match: {
            user: {
              $in: userIdsForAggr,
            },
            movie: {
              $nin: movieIdsForAggr,
            },
            saved: false,
            rating: { $gt: 6.5 },
          },
        },
        {
          $group: {
            _id: '$movie',
            totalRating: { $sum: '$rating' },
            averageRating: { $avg: '$rating' },
            count: { $sum: 1 },
            users: { $push: '$user' },
          },
        },
        {
          $project: {
            _id: 0,
            movieId: '$_id',
            averageRating: '$averageRating',
            totalRating: '$totalRating',
            count: '$count',
            movie: '$movie',
            users: '$users',
          },
        },
        { $sort: { averageRating: -1 } },
        { $limit: 50 },
      ])
      .toArray()
    return discoverItems
  }
}
