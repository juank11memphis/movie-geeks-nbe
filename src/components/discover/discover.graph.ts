import { get } from 'lodash'
import { AuthenticationError } from 'apollo-server-lambda'

import { DiscoverItem, AppContext, AppContextLoaders, DBMovie, DiscoverResponse } from '../../types'

export const discoverType = `
  type DiscoverItem {
    id: String!
    movieId: String
    averageRating: Float
    totalRating: Float
    count: Float
    movie: Movie
  }

  type Discover {
    items: [DiscoverItem]
  }
`

const loadDiscoverItemMovie = async (
  discoverItem: DiscoverItem,
  loaders: AppContextLoaders,
): Promise<DBMovie> => loaders.movies.load(discoverItem.movieId)

export const discoverItemTypeResolvers = {
  id: (discoverItem: DiscoverItem): string => discoverItem.movieId,
  movie: (discoverItem: DiscoverItem, args: {}, { loaders }: AppContext): Promise<DBMovie> =>
    loadDiscoverItemMovie(discoverItem, loaders),
}

export const discoverQueryTypes = `
  discover: Discover
`

export const discoverQueriesResolvers = {
  discover: (
    root: {},
    args: {},
    { discoverService, currentUser }: AppContext,
  ): Promise<DiscoverResponse> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    const currentUserId = get(currentUser, '_id')
    return discoverService.discover(currentUserId)
  },
}
