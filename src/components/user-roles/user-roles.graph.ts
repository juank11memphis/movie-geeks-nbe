export const userRoleType = `
  type UserRole {
    name: String!
    permissions: [String]
  }
`
