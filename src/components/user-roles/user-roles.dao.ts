import { UserRoleModel } from './user-role'
import { DBUserRole } from '../../types'

export class UserRolesDao {
  async loadByIds(ids: string[]): Promise<DBUserRole[] | null> {
    const userRoles = await UserRoleModel.find({ _id: { $in: ids } })
    return userRoles
  }
}
