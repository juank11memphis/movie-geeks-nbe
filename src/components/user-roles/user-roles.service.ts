import { keyBy } from 'lodash'

import { UserRolesDao } from './user-roles.dao'
import { DBUserRole } from '../../types'

export class UserRolesService {
  userRolesDao: UserRolesDao

  constructor() {
    this.userRolesDao = new UserRolesDao()
  }

  async loadByIds(ids: string[]): Promise<DBUserRole[]> {
    const userRoles = await this.userRolesDao.loadByIds(ids)
    const userRolesById = keyBy(userRoles, '_id')
    const userRolesSorted = ids.map((userRoleId: string) => userRolesById[userRoleId])
    return userRolesSorted
  }
}
