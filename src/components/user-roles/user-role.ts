import mongoose, { Model } from 'mongoose'
import { DBUserRole } from '../../types'

const Schema = mongoose.Schema

const UserRolesSchema = new Schema(
  {
    name: String,
    permissions: Array,
  },
  {
    timestamps: true,
    collection: 'user-roles',
  },
)

let userRoleModel: Model<DBUserRole>
try {
  userRoleModel = mongoose.model<DBUserRole>('UserRoles')
} catch (error) {
  userRoleModel = mongoose.model<DBUserRole>('UserRoles', UserRolesSchema)
}

export { userRoleModel as UserRoleModel }
