import mongoose, { Model } from 'mongoose'
import { DBMovie } from '../../types'

const Schema = mongoose.Schema

const MovieSchema = new Schema(
  {
    title: String,
    originalTitle: String,
    posterPath: String,
    genres: Array,
    overview: String,
    releaseDate: String,
    year: String,
    tmdbId: String,
    ratingsData: {}, // { globalRating, ratingsCount, ratingsSum }
    youtubeTrailerKey: String,
  },
  {
    timestamps: true,
    collection: 'movies',
  },
)

let movieModel: Model<DBMovie>
try {
  movieModel = mongoose.model<DBMovie>('Movie')
} catch (error) {
  movieModel = mongoose.model<DBMovie>('Movie', MovieSchema)
}

export { movieModel as MovieModel }
