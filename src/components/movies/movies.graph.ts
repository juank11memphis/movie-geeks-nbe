import { get } from 'lodash'

import { AppContext, UserMovie, GetYoutubeOfficialTrailerKeyQueryArgs } from '../../types'

export const movieType = `
  type Movie {
    id: String!
    title: String
    originalTitle: String
    posterPath: String
    genres: [String]
    overview: String
    releaseDate: String
    year: String
    youtubeTrailerKey: String
  }
  input MovieFilters {
    genres: [String]
    minimumRating: Float
    yearsRange: [Float]
  }
  input MovieParams {
    filters: MovieFilters
    paging: PagingParams
  }
`

export const moviesQueryTypes = `
  getPlayingNow: [UserMovie]
  getYoutubeOfficialTrailerKey(movieId: String): String
`

export const moviesQueriesResolvers = {
  getPlayingNow: (
    root: {},
    args: {},
    { userMoviesService, currentUser }: AppContext,
  ): Promise<UserMovie[]> => {
    const currentUserId = get(currentUser, '_id')
    return userMoviesService.getPlayingNow(currentUserId)
  },
  getYoutubeOfficialTrailerKey: (
    root: {},
    { movieId }: GetYoutubeOfficialTrailerKeyQueryArgs,
    { moviesService }: AppContext,
  ): Promise<string> => moviesService.getYoutubeOfficialTrailerKey(movieId),
}
