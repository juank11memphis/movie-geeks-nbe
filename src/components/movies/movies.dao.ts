import { MovieModel } from './movie'
import { Movie, DBMovie } from '../../types'

export class MoviesDao {
  async loadByIds(ids: string[]): Promise<DBMovie[]> {
    const movies = await MovieModel.find({ _id: { $in: ids } })
    return movies
  }

  create(movieData: Movie): Promise<DBMovie> {
    const newMovie = new MovieModel(movieData)
    return newMovie.save()
  }

  async update(dbMovie: DBMovie, data: Movie): Promise<DBMovie> {
    return dbMovie.updateOne(data)
  }

  async getById(id: string): Promise<DBMovie | null> {
    const movie = await MovieModel.findById(id)
    return movie
  }

  async getByTmdbId(tmdbId: string): Promise<DBMovie | null> {
    const movie = await MovieModel.findOne({ tmdbId: tmdbId })
    return movie
  }

  async findByTmdbIds(ids: string[]): Promise<DBMovie[]> {
    const movies = await MovieModel.find({
      tmdbId: {
        $in: ids,
      },
    })
    return movies
  }
}
