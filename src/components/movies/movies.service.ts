import { difference, keyBy, map } from 'lodash'
import { ApolloError } from 'apollo-server-lambda'

import { MoviesDao } from './movies.dao'
import { isValidDBId, TheMovieDBApi } from '../../shared'
import { Movie, DBMovie } from '../../types'

export class MoviesService {
  moviesDao: MoviesDao
  themoviedbApi: TheMovieDBApi

  constructor() {
    this.moviesDao = new MoviesDao()
    this.themoviedbApi = new TheMovieDBApi()
  }

  async loadByIds(ids: string[]): Promise<DBMovie[]> {
    const movies = await this.moviesDao.loadByIds(ids)
    const moviesById = keyBy(movies, '_id')
    const moviesSorted = ids.map((movieId: string) => moviesById[movieId])
    return moviesSorted
  }

  create(movieData: Movie): Promise<DBMovie> {
    return this.moviesDao.create(movieData)
  }

  async update(dbMovie: DBMovie, updateData: Movie): Promise<DBMovie> {
    await this.moviesDao.update(dbMovie, updateData)
    return this.getById(dbMovie._id)
  }

  getPlayingNow = async (): Promise<DBMovie[]> => {
    const tmdbMovies = await this.themoviedbApi.getPlayingNow()
    const dbMovies = await this.saveAllTMDBMovies(tmdbMovies)
    return dbMovies
  }

  saveAllTMDBMovies = async (tmdbMovies: Movie[] = []): Promise<DBMovie[]> => {
    console.time('moviesService.saveAllTMDBMovies')
    const tmdbMoviesMap = keyBy(tmdbMovies, 'id')
    const tmdbIds = tmdbMovies.map(tmdbMovie => tmdbMovie.id + '')
    const alreadyExistingMovies = await this.moviesDao.findByTmdbIds(tmdbIds)
    const alreadyExistingMoviesMap = keyBy(alreadyExistingMovies, 'tmdbId')
    const alreadyExistingTmdbIds = map(alreadyExistingMovies, 'tmdbId')
    const newTmdbIds = difference(tmdbIds, alreadyExistingTmdbIds)
    const dbMovies = []
    for (const tmdbId of tmdbIds) {
      const isNew = newTmdbIds.indexOf(tmdbId) > -1
      if (isNew) {
        const newMovie = tmdbMoviesMap[tmdbId]
        const { id, ...rest } = newMovie
        const dbMovie = await this.create({ tmdbId: id, ...rest })
        dbMovies.push(dbMovie)
      } else {
        dbMovies.push(alreadyExistingMoviesMap[tmdbId])
      }
    }
    console.timeEnd('moviesService.saveAllTMDBMovies')
    return dbMovies
  }

  async getById(id: string): Promise<DBMovie> {
    const movie = await this.moviesDao.getById(id)
    if (!movie) {
      throw new ApolloError(`Movie with id ${id} not found in database`)
    }
    return movie
  }

  async getByTmdbId(tmdbId: string): Promise<DBMovie> {
    const movie = await this.moviesDao.getByTmdbId(tmdbId + '')
    if (!movie) {
      throw new ApolloError(`Movie with tmdb ${tmdbId} not found in database`)
    }
    return movie
  }

  getByIdOrTmdbId(id: string): Promise<DBMovie> {
    if (isValidDBId(id)) {
      return this.getById(id)
    }
    return this.getByTmdbId(id)
  }

  async getYoutubeOfficialTrailerKey(id: string): Promise<string> {
    const dbMovie = await this.getById(id)
    if (dbMovie.youtubeTrailerKey) {
      return dbMovie.youtubeTrailerKey
    }
    const key = await this.themoviedbApi.getOfficialTrailerYoutubeKey(dbMovie.tmdbId)
    await this.update(dbMovie, { youtubeTrailerKey: key })
    return key
  }
}
