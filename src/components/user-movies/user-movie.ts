import mongoose, { Model } from 'mongoose'

import { DBUserMovie } from '../../types'

const Schema = mongoose.Schema

const UserMovieSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    movie: {
      type: Schema.Types.ObjectId,
      ref: 'Movie',
    },
    rating: Number,
    saved: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    collection: 'user-movies',
  },
)

let userMovieModel: Model<DBUserMovie>
try {
  userMovieModel = mongoose.model<DBUserMovie>('UserMovie')
} catch (error) {
  userMovieModel = mongoose.model<DBUserMovie>('UserMovie', UserMovieSchema)
}

export { userMovieModel as UserMovieModel }
