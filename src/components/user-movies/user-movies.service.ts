import { get, keyBy, isNil } from 'lodash'
import { ApolloError } from 'apollo-server-lambda'

import { UserMoviesDao } from './user-movies.dao'
import { MoviesService } from '../movies/movies.service'
import { SearchService } from '../search/search.service'
import { UsersService } from '../users/users.service'
import {
  getValidRatingValue,
  recalculateRatingsData,
  recalculateRatingsDataOnRemoveRating,
} from '../../shared'
import {
  UserMovie,
  PagingAndFilters,
  DBUserMovie,
  DBUser,
  DBMovie,
  UserMoviesSearchResponse,
  UserMoviesPaginated,
  UserMovieUpdateData,
  Movie,
} from '../../types'
import { UserUsersService } from '../user-users/user-users.service'
// import {
//   similarsUsersScoreRatingModifiedQueue,
//   similarsUsersScoreNewRatingQueue,
//   similarsUsersScoreRatingDeletedQueue,
// } from '../../core/jobs'

export class UserMoviesService {
  userMoviesDao: UserMoviesDao
  usersService: UsersService
  moviesService: MoviesService
  searchService: SearchService
  userUsersService: UserUsersService

  constructor() {
    this.userMoviesDao = new UserMoviesDao()
    this.usersService = new UsersService()
    this.moviesService = new MoviesService()
    this.searchService = new SearchService()
    this.userUsersService = new UserUsersService()
  }
  create(data: UserMovie): Promise<DBUserMovie> {
    return this.userMoviesDao.create(data)
  }

  async getById(userMovieId: string): Promise<DBUserMovie> {
    const userMovie = await this.userMoviesDao.getById(userMovieId)
    if (!userMovie) {
      throw new ApolloError(`UserMovie with id ${userMovieId} not found in database`)
    }
    return userMovie
  }

  async update(dbUserMovie: DBUserMovie, data: UserMovieUpdateData): Promise<DBUserMovie> {
    await this.userMoviesDao.update(dbUserMovie, data)
    return this.getById(dbUserMovie._id)
  }

  async getByUserAndMovie(userId: string, movieId: string): Promise<DBUserMovie> {
    const userMovie = await this.userMoviesDao.getByUserAndMovie(userId, movieId)
    if (!userMovie) {
      throw new ApolloError(
        `UserMovie with userId ${userId} and movieId ${movieId} not found in database`,
      )
    }
    return userMovie
  }

  validateUserAndMovie = async (
    userId: string,
    movieId: string,
  ): Promise<{ userDetail: DBUser; movieDetail: DBMovie; movie: string }> => {
    const userDetail = await this.usersService.getDetailById(userId)
    const movieDetail = await this.moviesService.getById(movieId)
    return { userDetail, movieDetail, movie: movieId }
  }

  async getPlayingNow(userId: string): Promise<UserMovie[]> {
    const movies = await this.moviesService.getPlayingNow()
    return this.getByUserAndMovies(movies, userId)
  }

  async search(query: string, userId: string): Promise<UserMoviesSearchResponse> {
    const searchResult = await this.searchService.search(query)
    const userMovies = await this.getByUserAndMovies(searchResult.movies, userId)
    delete searchResult.movies
    return {
      ...searchResult,
      userMovies: userMovies,
    }
  }

  async getByUserAndMovies(movies: DBMovie[], userId?: string): Promise<UserMovie[]> {
    if (!userId) {
      return movies.map(movie => this.groomUserMovieObject(movie))
    }
    const movieIds = movies.map(movie => movie._id)
    const userMovies = await this.userMoviesDao.getByUserAndMovies(userId, movieIds)
    const userMoviesByMovieId = keyBy(userMovies, 'movie')
    return movies.map(movie =>
      this.groomUserMovieObject(movie, userId, userMoviesByMovieId[movie._id]),
    )
  }

  groomUserMovieObject(movie: DBMovie | Movie, userId = '', userMovie?: UserMovie): UserMovie {
    const some = {
      user: userId || '',
      movie: get(movie, '_id') || get(movie, 'id'),
      movieDetail: {
        id: get(movie, '_id') || get(movie, 'id'),
        ...((movie as DBMovie).toObject ? (movie as DBMovie).toObject() : movie),
      },
      rating: get(userMovie, 'rating', undefined),
      saved: get(userMovie, 'saved', false),
    }
    return some
  }

  addComparandUserData(userMovies: UserMovie[], comparandUserMovies: UserMovie[]): UserMovie[] {
    const userMoviesMap = keyBy(userMovies, 'movie')
    const finalUserMovies = comparandUserMovies.map(comparandUserMovie => {
      const userMovie = userMoviesMap[get(comparandUserMovie, 'movie', '')]
      const movieDetail = isNil(get(userMovie, 'movieDetail.title'))
        ? comparandUserMovie.movieDetail
        : userMovie.movieDetail
      const userId = comparandUserMovie.user || ''
      return {
        ...userMovie,
        movieDetail,
        comparandUser: {
          userId: userId.toString(),
          rating: comparandUserMovie.rating,
          saved: comparandUserMovie.saved,
        },
      }
    })
    return finalUserMovies
  }

  async getComparandUserMovies(
    userMovies: UserMovie[],
    comparandUserId?: string,
  ): Promise<UserMovie[]> {
    const movies = userMovies.map(userMovie => userMovie.movieDetail)
    const movieIds = movies.map(movie => get(movie, 'id') || get(movie, '_id'))
    const dbMovies = await this.moviesService.loadByIds(movieIds)
    return this.getByUserAndMovies(dbMovies, comparandUserId)
  }

  loadWatchlist = async (
    mainUserId?: string,
    comparandUserId?: string,
    { filters, paging }: PagingAndFilters = {},
  ): Promise<UserMoviesPaginated> => {
    const watchlistResult = await this.userMoviesDao.loadWatchlist(mainUserId, filters, paging)
    const { userMovies: mainUserMovies } = watchlistResult
    if (!mainUserMovies || mainUserMovies.length <= 0) {
      return { userMovies: [], pagingMetadata: { next: undefined, previous: undefined } }
    }
    const comparandUserMovies = await this.getComparandUserMovies(mainUserMovies, comparandUserId)
    const finalUserMovies = this.addComparandUserData(mainUserMovies, comparandUserMovies)
    return { ...watchlistResult, userMovies: finalUserMovies }
  }

  async loadWatchlistAs(
    mainUserId?: string,
    impersonateUserId?: string,
    { filters, paging }: PagingAndFilters = {},
  ): Promise<UserMoviesPaginated> {
    const result = await this.userMoviesDao.loadWatchlist(impersonateUserId, filters, paging)
    const { userMovies: impersonateUserMovies } = result
    if (!impersonateUserMovies || impersonateUserMovies.length <= 0) {
      return { userMovies: [], pagingMetadata: { next: undefined, previous: undefined } }
    }
    const mainUserMovies = await this.getComparandUserMovies(impersonateUserMovies, mainUserId)
    const finalUserMovies = this.addComparandUserData(mainUserMovies, impersonateUserMovies)
    return { ...result, userMovies: finalUserMovies }
  }

  loadCollection = async (
    mainUserId?: string,
    comparandUserId?: string,
    { filters, paging }: PagingAndFilters = {},
  ): Promise<UserMoviesPaginated> => {
    const result = await this.userMoviesDao.loadCollection(mainUserId, filters, paging)
    const { userMovies: mainUserMovies } = result
    if (!mainUserMovies || mainUserMovies.length <= 0) {
      return { userMovies: [], pagingMetadata: { next: undefined } }
    }
    const comparandUserMovies = await this.getComparandUserMovies(mainUserMovies, comparandUserId)
    const finalUserMovies = this.addComparandUserData(mainUserMovies, comparandUserMovies)
    return { ...result, userMovies: finalUserMovies }
  }

  async loadCollectionAs(
    mainUserId?: string,
    impersonateUserId?: string,
    { filters, paging }: PagingAndFilters = {},
  ): Promise<UserMoviesPaginated> {
    const result = await this.userMoviesDao.loadCollection(impersonateUserId, filters, paging)
    const { userMovies: impersonateUserMovies } = result
    if (!impersonateUserMovies || impersonateUserMovies.length <= 0) {
      return { userMovies: [], pagingMetadata: { next: undefined, previous: undefined } }
    }
    const mainUserMovies = await this.getComparandUserMovies(impersonateUserMovies, mainUserId)
    const finalUserMovies = this.addComparandUserData(mainUserMovies, impersonateUserMovies)
    return { ...result, userMovies: finalUserMovies }
  }

  addToWatchlist = async (userId: string, movieId: string): Promise<DBUserMovie> => {
    await this.validateUserAndMovie(userId, movieId)
    try {
      const dbUserMovie = await this.getByUserAndMovie(userId, movieId)
      return this.update(dbUserMovie, { saved: true })
    } catch (error) {
      return this.create({
        user: userId,
        movie: movieId,
        saved: true,
      })
    }
  }

  removeFromWatchlist = async (userId: string, movieId: string): Promise<DBUserMovie> => {
    const dbUserMovie = await this.getByUserAndMovie(userId, movieId)
    return this.update(dbUserMovie, { saved: false })
  }

  rateMovie = async (userId: string, movieId: string, rating: number): Promise<DBUserMovie> => {
    await this.validateUserAndMovie(userId, movieId)
    let currentUserMovie
    try {
      currentUserMovie = await this.getByUserAndMovie(userId, movieId)
    } catch (error) {
      currentUserMovie = undefined
    }
    const oldRating = currentUserMovie ? currentUserMovie.rating : undefined
    const newRating = getValidRatingValue(rating)
    const userMovieData = { rating: newRating, saved: false }

    await this.createOrUpdate(userId, movieId, userMovieData, currentUserMovie)
    await this.recalculateMovieRatingsData(movieId, oldRating, newRating)

    await this.executePostRatingProcesses(userId, movieId, oldRating, newRating)

    return this.getByUserAndMovie(userId, movieId)
  }

  async recalculateMovieRatingsData(
    movieId: string,
    oldRating?: number,
    newRating?: number,
  ): Promise<void> {
    const movie = await this.moviesService.getById(movieId)
    const ratingsData = recalculateRatingsData(movie, oldRating, newRating)
    await this.moviesService.update(movie, { ratingsData })
  }

  async executePostRatingProcesses(
    userId: string,
    movieId: string,
    oldRating?: number,
    newRating?: number,
  ): Promise<void> {
    if (oldRating) {
      await this.executePostRatingModifiedProcess(userId, movieId, newRating)
    } else {
      await this.executePostNewRatingProcess(userId, movieId)
    }
  }

  async executePostRatingModifiedProcess(
    userId: string,
    movieId: string,
    newRating?: number,
  ): Promise<void> {
    const userMovie = await this.getByUserAndMovie(userId, movieId)
    const usersWithSameMovie = await this.getUsersWithSameMovie(userId, movieId)
    await this.userUsersService.processUsersWithSameMovieRatingModified(
      userMovie,
      usersWithSameMovie,
      newRating,
    )
  }

  async executePostRatingRemovedProcess(userId: string, movieId: string): Promise<void> {
    const userMovie = await this.getByUserAndMovie(userId, movieId)
    const usersWithSameMovie = await this.userUsersService.getUsersWithSameMovie(userId, movieId)
    await this.userUsersService.processUsersWithSameMovieRatingDeleted(
      userMovie,
      usersWithSameMovie,
    )
  }

  async executePostNewRatingProcess(userId: string, movieId: string): Promise<void> {
    const userMovie = await this.getByUserAndMovie(userId, movieId)
    const usersWithSameMovie = await this.getUsersWithSameMovie(userId, movieId)
    await this.userUsersService.processUsersWithSameMovieNewRating(userMovie, usersWithSameMovie)
  }

  removeMovieRating = async (userId: string, movieId: string): Promise<DBUserMovie> => {
    const { movieDetail } = await this.validateUserAndMovie(userId, movieId)
    const dbUserMovie = await this.getByUserAndMovie(userId, movieDetail._id)
    const currentRating = get(dbUserMovie, 'rating', undefined)
    if (!dbUserMovie || !currentRating) {
      return dbUserMovie
    }
    const ratingsData = recalculateRatingsDataOnRemoveRating(movieDetail, currentRating)
    await this.update(dbUserMovie, { saved: false, rating: undefined })
    await this.moviesService.update(movieDetail, { ratingsData })
    await this.executePostRatingRemovedProcess(userId, movieId)
    return this.getByUserAndMovie(userId, movieId)
  }

  createOrUpdate = async (
    userId: string,
    movieId: string,
    data: UserMovieUpdateData,
    currentUserMovie?: DBUserMovie,
  ): Promise<DBUserMovie> => {
    if (currentUserMovie === undefined) {
      return this.userMoviesDao.create({
        user: userId,
        movie: movieId,
        ...data,
      })
    }
    return this.userMoviesDao.update(currentUserMovie, { ...data })
  }

  getUsersWithSameMovie(userId: string, movieId: string): Promise<DBUserMovie[]> {
    return this.userMoviesDao.getUsersWithSameMovie(userId, movieId)
  }

  getFullCollection(userId: string): Promise<DBUserMovie[]> {
    return this.userMoviesDao.getFullCollection(userId)
  }

  getFullWatchlist(userId: string): Promise<DBUserMovie[]> {
    return this.userMoviesDao.getFullWatchlist(userId)
  }
}
