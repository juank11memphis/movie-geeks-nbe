import { AuthenticationError } from 'apollo-server-lambda'
import { get } from 'lodash'

import {
  DBUserMovie,
  UserMovie,
  Movie,
  ComparandUser,
  AppContext,
  UserMoviesPaginated,
  UserMoviesQueryArgs,
  UserMoviesMutationArgs,
} from '../../types'

export const userMovieType = `
  type UserMoviesResponse {
    userMovies: [UserMovie]
    pagingMetadata: PagingMetadata
  }
  type UserMovieComparand {
    userId: String
    rating: Float
    saved: Boolean
  }
  type UserMovie {
    id: String!
    userId: String
    movieId: String
    rating: Float
    saved: Boolean
    movie: Movie
    comparandUser: UserMovieComparand
  }
`

export const userMovieTypeResolver = {
  id: (userMovie: DBUserMovie): string =>
    `${userMovie.user.toString()}-${userMovie.movie.toString()}`,
  userId: (userMovie: DBUserMovie): string | null => {
    if (userMovie.user) {
      return userMovie.user.toString()
    }
    return null
  },
  movieId: (userMovie: DBUserMovie): string => userMovie.movie.toString(),
  movie: (userMovie: UserMovie): Movie => ({
    id: get(userMovie.movieDetail, 'id') || get(userMovie.movieDetail, '_id', ''),
    ...userMovie.movieDetail,
  }),
  comparandUser: ({ comparandUser }: UserMovie): ComparandUser | null =>
    comparandUser && comparandUser.userId ? comparandUser : null,
}

export const userMoviesQueryTypes = `
  loadWatchlist(mainUserId:String, comparandUserId: String, params: MovieParams): UserMoviesResponse
  loadWatchlistAs(mainUserId:String, impersonateUserId: String, params: MovieParams): UserMoviesResponse
  loadCollection(mainUserId:String, comparandUserId: String, params: MovieParams): UserMoviesResponse
  loadCollectionAs(mainUserId: String, impersonateUserId: String, params: MovieParams): UserMoviesResponse
`

export const userMoviesQueryResolvers = {
  loadWatchlist: (
    root: {},
    { mainUserId, comparandUserId, params }: UserMoviesQueryArgs,
    { userMoviesService }: AppContext,
  ): Promise<UserMoviesPaginated> =>
    userMoviesService.loadWatchlist(mainUserId, comparandUserId, params),
  loadWatchlistAs: (
    root: {},
    { mainUserId, impersonateUserId, params }: UserMoviesQueryArgs,
    { userMoviesService }: AppContext,
  ): Promise<UserMoviesPaginated> =>
    userMoviesService.loadWatchlistAs(mainUserId, impersonateUserId, params),
  loadCollection: (
    root: {},
    { mainUserId, comparandUserId, params }: UserMoviesQueryArgs,
    { userMoviesService }: AppContext,
  ): Promise<UserMoviesPaginated> =>
    userMoviesService.loadCollection(mainUserId, comparandUserId, params),
  loadCollectionAs: (
    root: {},
    { mainUserId, impersonateUserId, params }: UserMoviesQueryArgs,
    { userMoviesService }: AppContext,
  ): Promise<UserMoviesPaginated> =>
    userMoviesService.loadCollectionAs(mainUserId, impersonateUserId, params),
}

export const userMoviesMutationTypes = `
  addToWatchlist(movieId: String): UserMovie
  removeFromWatchlist(movieId: String): UserMovie
  rateMovie(movieId: String, rating: Float): UserMovie
  removeMovieRating(movieId: String): UserMovie
`

export const userMoviesMutationsResolvers = {
  addToWatchlist: (
    root: {},
    { movieId }: UserMoviesMutationArgs,
    { userMoviesService, currentUser }: AppContext,
  ): Promise<DBUserMovie> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.addToWatchlist(currentUser._id, movieId)
  },
  removeFromWatchlist: (
    root: {},
    { movieId }: UserMoviesMutationArgs,
    { userMoviesService, currentUser }: AppContext,
  ): Promise<DBUserMovie> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.removeFromWatchlist(currentUser._id, movieId)
  },
  rateMovie: (
    root: {},
    { movieId, rating }: UserMoviesMutationArgs,
    { userMoviesService, currentUser }: AppContext,
  ): Promise<DBUserMovie> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.rateMovie(currentUser._id, movieId, rating)
  },
  removeMovieRating: (
    root: {},
    { movieId }: UserMoviesMutationArgs,
    { userMoviesService, currentUser }: AppContext,
  ): Promise<DBUserMovie> => {
    if (!currentUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return userMoviesService.removeMovieRating(currentUser._id, movieId)
  },
}
