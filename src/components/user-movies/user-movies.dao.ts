import mongodb from 'mongodb'
import MongoPaging from 'mongo-cursor-pagination'
import { get } from 'lodash'

import { UserMovieModel } from './user-movie'
import {
  UserMovie,
  DBUserMovie,
  PagingParams,
  MovieFilters,
  UserMoviesPaginated,
  UserMovieUpdateData,
} from '../../types'
import { constructUserMovieAggregate } from '../../shared'

export class UserMoviesDao {
  create(data: UserMovie): Promise<DBUserMovie> {
    const newUserMovie = new UserMovieModel(data)
    return newUserMovie.save()
  }

  async getById(userMovieId: string): Promise<DBUserMovie | null> {
    const userMovie = await UserMovieModel.findById(userMovieId)
    return userMovie
  }

  async update(dbUserMovie: DBUserMovie, data: UserMovieUpdateData): Promise<DBUserMovie> {
    return dbUserMovie.updateOne(data)
  }

  async getByUserAndMovie(userId: string, movieId: string): Promise<DBUserMovie | null> {
    const userMovie = await UserMovieModel.findOne({ user: userId, movie: movieId })
    return userMovie
  }

  async loadWatchlist(
    userId?: string,
    filters?: MovieFilters,
    paging?: PagingParams,
  ): Promise<UserMoviesPaginated> {
    const aggregationQuery = constructUserMovieAggregate({
      baseMatchQuery: { user: new mongodb.ObjectID(userId), saved: true },
      filters,
    })
    const { results, previous, hasPrevious, next, hasNext } = await MongoPaging.aggregate<
      DBUserMovie[]
    >(UserMovieModel.collection, {
      aggregation: aggregationQuery,
      limit: get(paging, 'limit'),
      next: get(paging, 'next'),
      paginatedField: 'updatedAt',
    })
    return {
      userMovies: results,
      pagingMetadata: {
        previous,
        hasPrevious,
        next,
        hasNext,
      },
    }
  }

  async loadCollection(
    userId?: string,
    filters?: MovieFilters,
    paging?: PagingParams,
  ): Promise<UserMoviesPaginated> {
    const aggregationQuery = constructUserMovieAggregate({
      baseMatchQuery: {
        user: new mongodb.ObjectID(userId),
        saved: false,
        rating: { $gt: -1 },
      },
      filters,
    })
    const { results, previous, hasPrevious, next, hasNext } = await MongoPaging.aggregate<
      DBUserMovie[]
    >(UserMovieModel.collection, {
      aggregation: aggregationQuery,
      limit: get(paging, 'limit'),
      next: get(paging, 'next'),
      paginatedField: 'rating',
    })

    return {
      userMovies: results,
      pagingMetadata: {
        previous,
        hasPrevious,
        next,
        hasNext,
      },
    }
  }

  async getByUserAndMovies(userId: string, movieIds: string[]): Promise<DBUserMovie[]> {
    const userMovies = await UserMovieModel.find({ user: userId, movie: { $in: movieIds } })
    return userMovies
  }

  async getUsersWithSameMovie(userId: string, movieId: string): Promise<DBUserMovie[]> {
    const userMovies = await UserMovieModel.find({
      user: { $not: { $eq: userId } },
      movie: movieId,
      saved: false,
      rating: { $gt: -1 },
    })
    return userMovies
  }

  async getFullCollection(userId: string): Promise<DBUserMovie[]> {
    const userMovies = await UserMovieModel.find({
      user: userId,
      saved: false,
      rating: { $gt: -1 },
    })
    return userMovies
  }

  async getFullWatchlist(userId: string): Promise<DBUserMovie[]> {
    const userMovies = await UserMovieModel.find({
      user: userId,
      saved: true,
    })
    return userMovies
  }
}
