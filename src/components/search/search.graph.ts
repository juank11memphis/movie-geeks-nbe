import { get } from 'lodash'

import { AppContext, SearchQueryArgs, UserMoviesSearchResponse } from '../../types'

export const searchType = `
  type Search {
    page: Int
    totalResults: Int
    totalPages: Int
    userMovies: [UserMovie]
  }
`

export const searchQueryTypes = `
  search(query: String): Search
`

export const searchQueriesResolvers = {
  search: (
    root: {},
    { query }: SearchQueryArgs,
    { userMoviesService, currentUser }: AppContext,
  ): Promise<UserMoviesSearchResponse> => userMoviesService.search(query, get(currentUser, '_id')),
}
