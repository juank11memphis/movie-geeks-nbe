import { TheMovieDBApi } from '../../shared'
import { MoviesService } from '../movies/movies.service'
import { SearchResponse } from '../../types'

export class SearchService {
  themoviedbApi: TheMovieDBApi
  moviesService: MoviesService

  constructor() {
    this.themoviedbApi = new TheMovieDBApi()
    this.moviesService = new MoviesService()
  }

  search = async (query = ''): Promise<SearchResponse> => {
    if (!query || query === '') {
      return {
        movies: [],
        totalResults: 0,
        totalPages: 0,
        page: 0,
      }
    }
    const searchResult = await this.themoviedbApi.searchMovies(query)
    const { movies, ...rest } = searchResult
    const dbMovies = await this.moviesService.saveAllTMDBMovies(movies)
    return {
      movies: dbMovies,
      ...rest,
    }
  }
}
