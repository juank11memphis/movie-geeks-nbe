import { UserInputError } from 'apollo-server-lambda'

import {
  generateToken,
  generateResetPasswordToken,
  comparePasswords,
  hashPassword,
} from '../../shared/util/auth'
import { UsersService } from '../users/users.service'
import { User, DecodedToken, AuthResponse, DBUser } from '../../types'
import { getValue, MailgunUtil } from '../../shared'

export class AuthService {
  usersService: UsersService

  constructor() {
    this.usersService = new UsersService()
  }

  async signin(email: string, password: string): Promise<AuthResponse> {
    const user = await this.usersService.getDetailByEmail(email)
    if (!comparePasswords(password, user.password)) {
      throw new UserInputError('This password is not correct')
    }
    const token = generateToken(user._id, user.roles)
    return {
      user,
      token,
    }
  }

  async signup(user: User): Promise<AuthResponse> {
    const hasPassword = user.password ? true : false
    // first save is just to perform validations
    let savedUser = await this.usersService.create({ ...user, hasPassword })
    // after having a valid record we encrypt the password and update it
    const encryptedPassword = hashPassword(user.password)
    await this.usersService.rawUpdateUser(savedUser._id, { password: encryptedPassword })
    savedUser = await this.usersService.getDetailById(savedUser._id)
    const token = generateToken(savedUser._id, savedUser.roles)
    void MailgunUtil.sendWelcomeEmail(savedUser)
    return {
      user: savedUser,
      token,
    }
  }

  async authSocial(user: User): Promise<AuthResponse> {
    const userData = { ...user, hasPassword: false }
    const { isNewUser, dbUser } = await this.usersService.getByEmailOrCreate(user.email, userData)
    if (isNewUser) {
      void MailgunUtil.sendWelcomeEmail(dbUser)
    }
    const token = generateToken(dbUser._id, dbUser.roles)
    return {
      user: dbUser,
      token,
    }
  }

  async requestPasswordReset(email: string): Promise<DBUser> {
    const user = await this.usersService.getDetailByEmail(email)
    const token = generateResetPasswordToken(user._id)
    const link = getValue('reset_password_email_link')
    void MailgunUtil.sendResetPasswordEmail(user, `${link}?token=${token}`)
    return user
  }

  resetPassword = async (userId: string, password: string): Promise<AuthResponse> => {
    const user = await this.usersService.getDetailById(userId)
    // first update is just to validate the password is valid
    await user.updateOne({ password })
    const encryptedPassword = hashPassword(password)
    // after having a valid password we encrypt it and update again
    await user.updateOne({
      password: encryptedPassword,
      hasPassword: true,
    })
    const updatedUser = await this.usersService.getDetailById(user.id)
    const newToken = generateToken(updatedUser._id, updatedUser.roles)
    return {
      user: updatedUser,
      token: newToken,
    }
  }

  async refreshToken(tokenData: DecodedToken): Promise<AuthResponse> {
    const user = await this.usersService.getDetailById(tokenData.userId)
    const token = generateToken(user._id, user.roles)
    return {
      token,
      user,
    }
  }
}
