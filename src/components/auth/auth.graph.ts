import { AuthenticationError } from 'apollo-server-lambda'

import {
  AppContext,
  AuthResponse,
  SignupMutationArgs,
  SigninMutationArgs,
  DBUser,
  RequestPasswordResetMutationArgs,
  ResetPasswordMutationArgs,
} from '../../types'

export const authType = `
  type AuthResponse {
    user: User!
    token: String!
  }
`

export const authMutationTypes = `
  signin(email: String, password: String): AuthResponse
  signup(user: UserInput): AuthResponse
  authSocial(user: UserInput): AuthResponse
  requestPasswordReset(email: String): User
  resetPassword(password: String): AuthResponse
  refreshToken: AuthResponse
`

export const authMutationsResolvers = {
  signin: async (
    root: {},
    { email, password }: SigninMutationArgs,
    { authService }: AppContext,
  ): Promise<AuthResponse> => authService.signin(email, password),
  signup: async (
    root: {},
    { user }: SignupMutationArgs,
    { authService }: AppContext,
  ): Promise<AuthResponse> => authService.signup(user),
  authSocial: (
    root: {},
    { user }: SignupMutationArgs,
    { authService }: AppContext,
  ): Promise<AuthResponse> => authService.authSocial(user),
  requestPasswordReset: (
    root: {},
    { email }: RequestPasswordResetMutationArgs,
    { authService }: AppContext,
  ): Promise<DBUser> => authService.requestPasswordReset(email),
  resetPassword: (
    root: {},
    { password }: ResetPasswordMutationArgs,
    { authService, resetPasswordUser }: AppContext,
  ): Promise<AuthResponse> => {
    if (!resetPasswordUser) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return authService.resetPassword(resetPasswordUser._id, password)
  },
  refreshToken: (
    root: {},
    args: {},
    { currentUser, tokenData, authService }: AppContext,
  ): Promise<AuthResponse> => {
    if (!currentUser || !tokenData) {
      throw new AuthenticationError('Unauthorized!!')
    }
    return authService.refreshToken(tokenData)
  },
}
