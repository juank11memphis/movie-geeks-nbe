import mongoose, { Model } from 'mongoose'
import { DBUserUsers } from '../../types'

const Schema = mongoose.Schema

const UserUsersSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    comparandUser: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    score: Number,
    movies: Array,
  },
  {
    timestamps: true,
    collection: 'user-users',
  },
)

let userUsersModel: Model<DBUserUsers>
try {
  userUsersModel = mongoose.model<DBUserUsers>('UserUsers')
} catch (error) {
  userUsersModel = mongoose.model<DBUserUsers>('UserUsers', UserUsersSchema)
}

export { userUsersModel as UserUsersModel }
