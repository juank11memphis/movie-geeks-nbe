import { UserUsersModel } from './user-users'
import { UserUsers, DBUserUsers } from '../../types'

export class UserUsersDao {
  async getByUsers(userId?: string, comparandUserId?: string): Promise<DBUserUsers | null> {
    const userUsers = await UserUsersModel.findOne({
      user: userId,
      comparandUser: comparandUserId,
    })
    return userUsers
  }

  async getTopSimilarUsers(userId: string, limit: number): Promise<DBUserUsers[]> {
    const similarUsers = await UserUsersModel.find({
      $or: [{ user: userId }, { comparandUser: userId }],
    })
      .sort({
        score: -1,
      })
      .limit(limit)
    return similarUsers
  }

  create(data: UserUsers): Promise<DBUserUsers> {
    const newRecord = new UserUsersModel(data)
    return newRecord.save()
  }

  async getUsersWithSameMovie(userId: string, movieId: string): Promise<DBUserUsers[]> {
    const usersWithSameMovie = await UserUsersModel.find({
      $or: [{ user: userId }, { comparandUser: userId }],
      movies: {
        $elemMatch: { id: movieId },
      },
    })
    return usersWithSameMovie
  }
}
