import { Document } from 'mongoose'
import { get, keyBy } from 'lodash'

import { UserUsersDao } from './user-users.dao'
import { UserUsersModel } from './user-users'
import { updateMany, removeMany, ensureType } from '../../shared'
import { UserUsers, UserMovie, DBUserUsers, SimilarUsersData } from '../../types'

export class UserUsersService {
  userUsersDao: UserUsersDao

  constructor() {
    this.userUsersDao = new UserUsersDao()
  }

  async getDetailedTopSimilarUsers(userId: string, limit: number): Promise<SimilarUsersData> {
    const topUserUsers = await this.getTopSimilarUsers(userId, limit)
    const moviesInCommonMap: { [id: string]: boolean } = {}
    const topUsersIds: string[] = []
    topUserUsers.forEach(userUsersRecord => {
      if (userUsersRecord.user + '' === userId + '') {
        topUsersIds.push(userUsersRecord.comparandUser + '')
      } else {
        topUsersIds.push(userUsersRecord.user + '')
      }
      userUsersRecord.movies.forEach(movie => (moviesInCommonMap[movie.id] = true))
    })
    const moviesInCommonIds = Object.keys(moviesInCommonMap)
    return {
      data: topUserUsers,
      moviesInCommonIds,
      userIds: topUsersIds,
    }
  }

  getTopSimilarUsers(userId: string, limit: number): Promise<DBUserUsers[]> {
    return this.userUsersDao.getTopSimilarUsers(userId, limit)
  }

  async getByUsers(userId?: string, comparandUserId?: string): Promise<DBUserUsers | null> {
    const record = await this.userUsersDao.getByUsers(userId, comparandUserId)
    if (!record) {
      return this.userUsersDao.getByUsers(comparandUserId, userId)
    }
    return record
  }

  async getByUsersAndMovie(
    userId: string,
    comparandUserId: string,
    movieId: string,
  ): Promise<DBUserUsers | null | undefined> {
    const record = await this.getByUsers(userId, comparandUserId)
    if (this.hasMovie(record, movieId)) {
      return record
    }
    return undefined
  }

  hasMovie(record: UserUsers | null, movieId: string): boolean {
    const movies = get(record, 'movies', [])
    const moviesById = keyBy(movies, 'id')
    if (moviesById[movieId]) {
      return true
    }
    return false
  }

  getRatingPoints(rating = 0, comparandRating = 0): number {
    let verySimilarLower = rating - 0.5
    verySimilarLower = verySimilarLower < 0 ? 0 : verySimilarLower
    let verySimilarHigher = rating + 0.5
    verySimilarHigher = verySimilarHigher > 10 ? 10 : verySimilarHigher
    if (comparandRating >= verySimilarLower && comparandRating <= verySimilarHigher) {
      return 2
    }
    let similarLower = verySimilarLower - 1
    similarLower = similarLower < 0 ? 0 : similarLower
    let similarHigher = verySimilarHigher + 1
    similarHigher = similarHigher > 10 ? 10 : similarHigher
    if (comparandRating >= similarLower && comparandRating <= similarHigher) {
      return 1
    }
    return 0
  }

  getUsersWithSameMovie(userId: string, movieId: string): Promise<DBUserUsers[]> {
    return this.userUsersDao.getUsersWithSameMovie(userId, movieId)
  }

  async processUsersWithSameMovieNewRating(
    userMovie: UserMovie,
    usersWithSameMovie: UserMovie[],
  ): Promise<void> {
    const bulkUpdates = []
    for (const comparandUserMovie of usersWithSameMovie) {
      const userUsersRecord = await this.getByUsers(userMovie.user, comparandUserMovie.user)
      const points = this.getRatingPoints(userMovie.rating, comparandUserMovie.rating)
      if (!userUsersRecord) {
        const data = {
          user: userMovie.user,
          comparandUser: comparandUserMovie.user,
          score: points,
          movies: [{ id: userMovie.movie, points: points }],
        }
        await this.userUsersDao.create(data)
      } else if (!this.hasMovie(userUsersRecord, userMovie.movie)) {
        const currentMovies = get(userUsersRecord, 'movies', [])
        const updateData = {
          ...userUsersRecord.toObject(),
          score: userUsersRecord.score + points,
          movies: [...currentMovies, { id: userMovie.movie, points: points }],
        }
        bulkUpdates.push({
          id: userUsersRecord._id,
          data: updateData,
        })
      }
    }
    await updateMany(UserUsersModel, bulkUpdates)
  }

  async processUsersWithSameMovieRatingModified(
    userMovie: UserMovie,
    usersWithSameMovie: UserMovie[],
    newRating?: number,
  ): Promise<void> {
    const bulkUpdates = []
    for (const comparandUserMovie of usersWithSameMovie) {
      const oldUserUsersRecord = await this.getByUsers(userMovie.user, comparandUserMovie.user)
      if (!oldUserUsersRecord) {
        continue
      }
      const oldUserUsersMovie = ensureType(
        oldUserUsersRecord.movies.find(movie => movie.id + '' === userMovie.movie + ''),
      )
      const oldPoints = oldUserUsersMovie.points
      const newPoints = this.getRatingPoints(newRating, comparandUserMovie.rating)
      const newMovies = oldUserUsersRecord.movies.filter(
        movie => movie.id + '' !== userMovie.movie + '',
      )
      const newUserUsersRecord = {
        ...oldUserUsersRecord.toObject(),
        movies: [...newMovies, { id: userMovie.movie, points: newPoints }],
        score: oldUserUsersRecord.score - oldPoints + newPoints,
      }
      bulkUpdates.push({
        id: oldUserUsersRecord._id,
        data: newUserUsersRecord,
      })
    }
    await updateMany(UserUsersModel, bulkUpdates)
  }

  async processUsersWithSameMovieRatingDeleted(
    userMovie: UserMovie,
    usersWithSameMovie: (UserUsers & Document)[],
  ): Promise<void> {
    const bulkUpdates = []
    const bulkDeletes = []
    for (const oldUserUsersRecord of usersWithSameMovie) {
      const oldUserUsersMovie = ensureType(
        oldUserUsersRecord.movies.find(movie => movie.id + '' === userMovie.movie + ''),
      )
      const oldPoints = oldUserUsersMovie.points
      const newMovies = oldUserUsersRecord.movies.filter(
        movie => movie.id + '' !== userMovie.movie + '',
      )
      const newUserUsersRecord = {
        ...oldUserUsersRecord.toObject(),
        movies: [...newMovies],
        score: oldUserUsersRecord.score - oldPoints,
      }
      if (newUserUsersRecord.movies.length > 0) {
        bulkUpdates.push({
          id: oldUserUsersRecord._id,
          data: newUserUsersRecord,
        })
      } else {
        bulkDeletes.push(oldUserUsersRecord._id)
      }
    }
    await updateMany(UserUsersModel, bulkUpdates)
    await removeMany(UserUsersModel, bulkDeletes)
  }
}
