import { UserModel } from '../users/user'
import { UserUsersService } from './user-users.service'
import { UserMoviesService } from '../user-movies/user-movies.service'
import { generalLogger } from '../../shared'
import { UserMovie } from '../../types'

const userMoviesService = new UserMoviesService()
const userUsersService = new UserUsersService()

const processUserCollection = async (fullCollection: UserMovie[]): Promise<void> => {
  for (const userMovie of fullCollection) {
    const usersWithSameMovie = await userMoviesService.getUsersWithSameMovie(
      userMovie.user,
      userMovie.movie,
    )
    await userUsersService.processUsersWithSameMovieNewRating(userMovie, usersWithSameMovie)
  }
}

const processCurrentData = async (): Promise<void> => {
  console.time('processCurrentData')
  generalLogger.info('Current data processor started...')
  const allUsers = await UserModel.find({})
  generalLogger.info(`Getting all users from database. Users count: ${allUsers.length}`)
  for (const user of allUsers) {
    const fullCollection = await userMoviesService.getFullCollection(user._id)
    // if (fullCollection.length > 10) {
    //   console.log(
    //     `${user.email} has rated more than 10 movies!!!! Collection length = ${fullCollection.length}`,
    //   )
    // }
    await processUserCollection(fullCollection)
  }
  console.timeEnd('processCurrentData')
}

export { processCurrentData }
