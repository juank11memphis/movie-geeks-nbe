resource "aws_sns_topic" "rating_added" {
  name = "${var.APP_NAME}-${var.ENVIRONMENT}-rating-added"
}

resource "aws_sns_topic" "rating_updated" {
  name = "${var.APP_NAME}-${var.ENVIRONMENT}-rating-updated"
}

resource "aws_sns_topic" "rating_removed" {
  name = "${var.APP_NAME}-${var.ENVIRONMENT}-rating-removed"
}
