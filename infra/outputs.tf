output "rating-added-topic" {
  value = "${aws_sns_topic.rating_added.arn}"
}

output "rating-updated-topic" {
  value = "${aws_sns_topic.rating_updated.arn}"
}

output "rating-removed-topic" {
  value = "${aws_sns_topic.rating_removed.arn}"
}
