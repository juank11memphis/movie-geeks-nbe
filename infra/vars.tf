variable "AWS_REGION" {
  default = "us-east-1"
}

variable "ENVIRONMENT" {
  default = "prd"
}

variable "APP_NAME" {
  default = "moviegeeks"
}
