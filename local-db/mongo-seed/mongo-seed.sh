#!/bin/bash
# setup-es.sh

echo 'seeding data into our database...'

mongoimport --host mongodb --port 27017 --db movie-geeks --username juanca --password password --authenticationDatabase admin --collection user-roles --type json --file data/user-roles.json --jsonArray
mongoimport --host mongodb --port 27017 --db movie-geeks --username juanca --password password --authenticationDatabase admin --collection users --type json --file data/users.json --jsonArray
mongoimport --host mongodb --port 27017 --db movie-geeks --username juanca --password password --authenticationDatabase admin --collection movies --type json --file data/movies.json --jsonArray
mongoimport --host mongodb --port 27017 --db movie-geeks --username juanca --password password --authenticationDatabase admin --collection user-movies --type json --file data/user-movies.json --jsonArray

echo 'data seeding complete...'
